/*
 * #%L
 * Nuiton Web :: Nuiton Web
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.filter;

import org.nuiton.topia.persistence.TopiaPersistenceContext;

/**
 * Implementation of the {@link TypedTopiaTransactionFilter} using the
 * {@link TopiaPersistenceContext} as type.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.6
 */
public abstract class TopiaTransactionFilter extends TypedTopiaTransactionFilter<TopiaPersistenceContext> {

    public TopiaTransactionFilter() {
        super(TopiaPersistenceContext.class);
    }

}
