/*
 * #%L
 * Nuiton Web :: Nuiton Web
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.web.jmx.ServletStatisticsService;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.lang.management.ManagementFactory;

/**
 * Monitoring filter is a simple non-intrusive servlet filter that collect
 * statistics about page computing time per request URI.
 *
 * All gathered data are published using JMX Bean to make it available through
 * monitoring tools such as jconsole.
 *
 * @since 1.8
 */
public class MonitoringFilter implements Filter {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MonitoringFilter.class);

    /** The objectName where the MBean will be registered. */
    protected ObjectName servletStatisticsMBeanName;

    /** The service used to record statistics before and after filter is called. */
    protected ServletStatisticsService servletStatisticsService;

    /**
     * Attach the MBean.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        try {
            MBeanServer server = ManagementFactory.getPlatformMBeanServer();
            // MBean implementation
            servletStatisticsService = new ServletStatisticsService();
            // Bind the stats to a SessionFactory
            // Register the Mbean on the server
            String applicationFqn = filterConfig.getInitParameter("applicationFqn");
            if (applicationFqn == null) {
                throw new IllegalArgumentException("You must provide the applicationFqn parameter in web.xml with a value such as com.mycompany.myapp");
            }
            servletStatisticsMBeanName = new ObjectName(applicationFqn + ":type=ServletStatistics");
            server.registerMBean(servletStatisticsService, servletStatisticsMBeanName);
            if (log.isInfoEnabled()) {
                log.info("mbean " + servletStatisticsService + " attached as "
                         + servletStatisticsMBeanName);
            }
        } catch (MalformedObjectNameException e) {
            log.error("unable to register mbean", e);
        } catch (InstanceAlreadyExistsException e) {
            log.error("unable to register mbean", e);
        } catch (MBeanRegistrationException e) {
            log.error("unable to register mbean", e);
        } catch (NotCompliantMBeanException e) {
            log.error("unable to register mbean", e);
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        servletStatisticsService.preFilter(servletRequest, servletResponse, filterChain);

        filterChain.doFilter(servletRequest, servletResponse);

        servletStatisticsService.postFilter(servletRequest, servletResponse, filterChain);

    }

    /**
     * Detach the MBean, print statistics.
     */
    @Override
    public void destroy() {

        if (log.isInfoEnabled()) {
            log.info("statistics:\n" + servletStatisticsService.toCsv());
        }

        try {
            MBeanServer server = ManagementFactory.getPlatformMBeanServer();
            server.unregisterMBean(servletStatisticsMBeanName);
            log.info("mbean detached " + servletStatisticsMBeanName);
        } catch (InstanceNotFoundException e) {
            log.error("unable to unregister mbean", e);
        } catch (MBeanRegistrationException e) {
            log.error("unable to unregister mbean", e);
        }
    }

}
