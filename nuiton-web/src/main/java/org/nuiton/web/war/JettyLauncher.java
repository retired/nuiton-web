/*
 * #%L
 * Nuiton Web :: Nuiton Web
 * %%
 * Copyright (C) 2011, 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.war;

import org.apache.jasper.runtime.JspFactoryImpl;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.webapp.WebAppContext;

import javax.servlet.jsp.JspFactory;
import javax.swing.ImageIcon;
import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.JarURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * War main class launcher (jetty based).
 * <p>
 * To use it :
 * java -jar app-xxx.war
 *
 * @author chatellier
 * @since 1.0
 */
public class JettyLauncher implements ActionListener, MouseListener {

    /** Jetty server instance. */
    protected Server jettyServer;

    /** Server URI. */
    protected URI serverUri;

    /** Default port. */
    protected int port = 8888;

    /**
     * Default context path.
     *
     * @since 1.13
     */
    protected String contextPath = "/";

    /**
     * Extrat suffix to add to the launch url (says http://localhost:{port}{contextPath}{startSuffixPath}
     *
     * @since 1.19
     */
    protected String startSuffixPath = "";

    /** Server name. */
    protected String serverName;

    /**
     * Main method (used by war in manifest).
     *
     * @param args args
     * @throws Exception
     */
    public static void main(String... args) throws Exception {
        JettyLauncher launcher = new JettyLauncher();
        launcher.configure(args);
        launcher.readInformation();
        launcher.startServer();
        launcher.installSystemTray();
        launcher.openBrowser();
    }

    /**
     * Parse WEB-INF/web.xml file and get server display name.
     *
     * @since 1.1.3
     */
    protected void readInformation() {
        InputStream stream = JettyLauncher.class.getResourceAsStream("/WEB-INF/web.xml");

        if (stream != null) {
            String content = readAsString(stream);
            if (content != null) {
                int first = content.indexOf("<display-name>");
                if (first >= 0) {
                    serverName = content.substring(first + 14, content.indexOf("</display-name>"));
                    System.out.println("Using server name : " + serverName);
                }
            }
        }

        // if none read, set default
        if (serverName == null || serverName.isEmpty()) {
            serverName = "Server";
        }
    }

    /**
     * Read input stream as string.
     * <p>
     * Code from commons io.
     *
     * @param stream stream to read
     * @return content as string
     * @since 1.1.3
     */
    protected String readAsString(InputStream stream) {
        InputStreamReader reader = new InputStreamReader(stream);
        StringWriter sw = new StringWriter();
        char[] buffer = new char[4096];
        int n = 0;
        try {
            while (-1 != (n = reader.read(buffer))) {
                sw.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sw.toString();
    }

    protected void configure(String... args) {

        for (int i = 0; i < args.length; i++) {
            if ("--port".equals(args[i]))
                port = Integer.parseInt(args[++i]);
            else if ("--path".equals(args[i])) {
                contextPath = args[++i];
                if (!contextPath.startsWith("/")) {
                    contextPath = "/" + contextPath;
                }
            }else if ("--start".equals(args[i])) {
                startSuffixPath = args[++i];
            }
        }
    }

    /**
     * Launch servlet container.
     *
     * @throws Exception
     */
    protected void startServer() throws Exception {
        System.out.println("Starting server embedded mode...");

        String fqnLauncherFile = JettyLauncher.class.getName().replaceAll("\\.", "/") + ".class";
        System.out.println("Search for launcher class : " + fqnLauncherFile);

        URL classFile = JettyLauncher.class.getClassLoader().getResource(fqnLauncherFile);
        System.out.println(" - using classFile : " + classFile);

        // strange following line seams also work for jnlp launch
        File me = new File(((JarURLConnection) classFile.openConnection()).getJarFile().getName());
        System.out.println(" - using warfile file : " + me);

        jettyServer = new Server(port);

        WebAppContext webappcontext = new WebAppContext();
        webappcontext.setContextPath(contextPath);
        webappcontext.setWar(me.getAbsolutePath());
        
        // fixes java.lang.IllegalStateException: No org.apache.tomcat.InstanceManager set in ServletContext
        // jetty 9.3.8 : webappcontext.setAttribute(InstanceManager.class.getName(), new SimpleInstanceManager());
        
        // fixes NPE due to getDefaultFactory() returning null
        JspFactory.setDefaultFactory(new JspFactoryImpl());

        HandlerCollection handlers = new HandlerCollection();
        handlers.setHandlers(new Handler[]{webappcontext, new DefaultHandler()});
        jettyServer.setHandler(handlers);

        jettyServer.start();

        // build server uri
        try {
            serverUri = new URI("http://localhost:" + port + contextPath + startSuffixPath);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /** Shutdown server. */
    protected void stopServer() {
        if (jettyServer != null) {
            try {
                jettyServer.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(0);
        }
    }

    /** Install system tray to stop server. */
    protected void installSystemTray() {
        if (SystemTray.isSupported()) {
            // build menu
            PopupMenu menu = new PopupMenu();
            MenuItem browserItem = new MenuItem("Start browser");
            browserItem.addActionListener(this);
            browserItem.setActionCommand("browser");
            menu.add(browserItem);

            MenuItem stopItem = new MenuItem("Stop server");
            stopItem.addActionListener(this);
            stopItem.setActionCommand("stop");
            menu.add(stopItem);

            // build tray icon
            URL imageURL = JettyLauncher.class.getResource("/favicon.png");
            if (imageURL == null) {
                imageURL = JettyLauncher.class.getResource("/favicon.jpg");
            }
            if (imageURL == null) {
                System.out.println("No favicon.{png|jpg} found, skip systray installation");
            } else {
                Image image = new ImageIcon(imageURL).getImage();
                TrayIcon icon = new TrayIcon(image, serverName, menu);
                icon.setImageAutoSize(true);
                icon.addMouseListener(this);

                // System tray
                SystemTray systemTray = SystemTray.getSystemTray();
                try {
                    systemTray.add(icon);
                } catch (AWTException ex) {
                    throw new RuntimeException("Can't install tray icon", ex);
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ("browser".equalsIgnoreCase(e.getActionCommand())) {
            openBrowser();
        } else if ("stop".equalsIgnoreCase(e.getActionCommand())) {
            stopServer();
        }
    }

    /**
     * Open browser.
     */
    protected void openBrowser() {
        if (Desktop.isDesktopSupported() && serverUri != null) {
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.BROWSE)) {
                System.out.println("Opening browser at " + serverUri);
                try {
                    desktop.browse(serverUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            openBrowser();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
