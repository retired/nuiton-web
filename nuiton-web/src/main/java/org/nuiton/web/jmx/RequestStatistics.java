/*
 * #%L
 * Nuiton Web :: Nuiton Web
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.jmx;

import java.io.Serializable;

/**
 * This pojo gather statistics for a given (not stored) request.
 *
 */
public class RequestStatistics implements Serializable {

    protected int count;

    protected long elapsedSum;

    protected long lowestElapsed = Long.MAX_VALUE;

    protected long highestElapsed;

    /** The number of time this Url was killed.
     * @return the total number of times this url was called
     */
    public int getCount() {
        return count;
    }

    /** The total time passed to process all the request.
     * @return a duration in milli-seconds.
     */
    public long getElapsedSum() {
        return elapsedSum;
    }

    /** The shortest time observed (duration of the fastest request)
     * @return a duration in milli-seconds.
     */
    public long getLowestElapsed() {
        return lowestElapsed;
    }

    /** The longest time observed (duration of the lowest request)
     * @return a duration in milli-seconds.
     */
    public long getHighestElapsed() {
        return highestElapsed;
    }

    /** The average time observed
     * @return a duration in milli-seconds.
     */
    public long getAverageElapsed() {
        return getElapsedSum() / getCount();
    }

    /** Each time a request is processed, this method should be called. It will
     * record the statistics for this request.
     *
     * @param start the time (in ms), when the request was queried by the user
     * @param stop the time (in ms), the request processing ended
     * @throws IllegalArgumentException if given times implies that stop time is
     * not after start
     */
    public void count(long start, long stop) {
        long elapsed = stop - start;
        if (elapsed < 0) {
            throw new IllegalArgumentException("a request can't be processed in less than 0 ms");
        }
        count += 1;
        elapsedSum += elapsed;
        if (elapsed < lowestElapsed) {
            lowestElapsed = elapsed;
        }
        if (elapsed > highestElapsed) {
            highestElapsed = elapsed;
        }
    }

    @Override
    public String toString() {
        return "count=" + count +
               ", elapsedSum=" + elapsedSum +
               ", lowestElapsed=" + lowestElapsed +
               ", highestElapsed=" + highestElapsed +
               ", averageElapsed=" + getAverageElapsed() +
               '}';
    }
}
