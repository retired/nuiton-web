/*
 * #%L
 * Nuiton Web :: Nuiton Web
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.jmx;

import java.util.Map;

/** Contract exposed to JMX exposing statistics gathered while the application
 * was used.
 *
 * It follows the convention imposed by JMX spec, objects used as parameters
 * or returned as value are {@link java.io.Serializable}. Contract name ends
 * with "MBean" while implementation not.
 */
public interface ServletStatisticsServiceMBean {

    /** The statistics gathered while the application was in use.
     *
     * @return a map with as key, the request Url, as value the statistics
     * gathered for this Url.
     */
    Map<String, RequestStatistics> getPerRequestStatistics();

    /** Reset statistics for all request. Statistics will acts like if the
     * monitored application was never used (0 request processed).
     */
    void reset();

    /** Get the statistics under the form of a CSV file suitable for
     * post-processing and use in a spreadsheet.
     *
     * @return the csv content. Seperator is ','. One line per entries returned
     * by {@link #getPerRequestStatistics()}. One column per attribute of
     * {@link RequestStatistics}.
     */
    String toCsv();
}
