/*
 * #%L
 * Nuiton Web :: Nuiton Web
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.jmx;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/** Implementation of {@link ServletStatisticsServiceMBean}.
 * 
 * For each request, it has a {@link RequestStatistics} instance and provide
 * an pre-filter and post-filter facade suitable for use in
 * {@link org.nuiton.web.filter.MonitoringFilter}.
 */
public class ServletStatisticsService implements ServletStatisticsServiceMBean {

    protected Map<ServletRequest, Long> requestStartTime =
            new HashMap<ServletRequest, Long>();

    protected Map<String, RequestStatistics> perRequestStatistics =
            new HashMap<String, RequestStatistics>();

    public void preFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {

        long start = System.currentTimeMillis();

        requestStartTime.put(servletRequest, start);

    }

    public void postFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {

        String contentType = servletResponse.getContentType();

        if (contentType != null && contentType.startsWith("text/html")) {

            long stop = System.currentTimeMillis();

            long start = requestStartTime.get(servletRequest);

            if (servletRequest instanceof HttpServletRequest) {
                String requestURI = ((HttpServletRequest) servletRequest).getRequestURI();
                RequestStatistics requestStatistics =
                        (RequestStatistics) perRequestStatistics.get(requestURI);
                if (requestStatistics == null) {
                    requestStatistics = new RequestStatistics();
                    perRequestStatistics.put(requestURI, requestStatistics);
                }
                requestStatistics.count(start, stop);
            }
        }

        requestStartTime.remove(servletRequest);
    }

    /** Implementation of the MBean contract.
     *
     * @see ServletStatisticsServiceMBean#getPerRequestStatistics()
     */
    @Override
    public Map<String, RequestStatistics> getPerRequestStatistics() {
        return perRequestStatistics;
    }

    /** Implementation of the MBean contract.
     *
     * @see ServletStatisticsServiceMBean#toCsv()
     */
    @Override
    public String toCsv() {
        StringBuilder csv = new StringBuilder();
        csv.append("PAGE,NOMBRE_DEMANDES,DUREE_TOTALE,DUREE_MOYENNE,DUREE_MIN,DUREE_MAX\n");
        for (Map.Entry<String, RequestStatistics> requestStatistics : perRequestStatistics.entrySet()) {
            String request = requestStatistics.getKey();
            RequestStatistics statistics = requestStatistics.getValue();
            csv.append(request).append(',')
               .append(statistics.getCount()).append(',')
               .append(statistics.getElapsedSum()).append(',')
               .append(statistics.getAverageElapsed()).append(',')
               .append(statistics.getLowestElapsed()).append(',')
               .append(statistics.getHighestElapsed()).append('\n');
        }
        return csv.toString();
    }

    /** Implementation of the MBean contract.
     *
     * @see ServletStatisticsServiceMBean#reset()
     */
    @Override
    public void reset() {
        perRequestStatistics.clear();
    }
}
