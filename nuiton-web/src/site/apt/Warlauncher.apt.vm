~~~
~~ #%L
~~ Nuiton Web :: Nuiton Web
~~ 
~~ $Id$
~~ $HeadURL$
~~ %%
~~ Copyright (C) 2011 CodeLutin, Chatellier Eric
~~ %%
~~ This program is free software: you can redistribute it and/or modify
~~ it under the terms of the GNU Lesser General Public License as 
~~ published by the Free Software Foundation, either version 3 of the 
~~ License, or (at your option) any later version.
~~ 
~~ This program is distributed in the hope that it will be useful,
~~ but WITHOUT ANY WARRANTY; without even the implied warranty of
~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
~~ GNU General Lesser Public License for more details.
~~ 
~~ You should have received a copy of the GNU General Lesser Public 
~~ License along with this program.  If not, see
~~ <http://www.gnu.org/licenses/lgpl-3.0.html>.
~~ #L%
~~~
                               ------
                             War launcher
                               ------
                               

War launcher

* Features

  * Start embedded servlet container with current webapp

  * Favicon support

  * Systray with popup menu

* Prerequisites

  By default, both jetty and winstone look for an icon named <<favicon.png>> or
  <<favicon.jpg>> at webapp base. If none of this icons is found, system tray
  won't work.
  
  In maven, by default, this file must be put in <<<src/main/webapp>>> directory.
  
  If <<<display-name>>> content can be read into <<<WEB-INF/web.xml>>> file,
  it's used as server name (currently in systray tooltip).

* Jetty based

** Maven configuration

  Add following dependencies to your project.

------------------------------------------------
<dependency>
    <groupId>org.nuiton.web</groupId>
    <artifactId>nuiton-web</artifactId>
    <version>${project.version}</version>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>org.eclipse.jetty</groupId>
    <artifactId>jetty-runner</artifactId>
    <version>${jettyVersion}</version>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>${servletApiVersion}</version>
    <scope>provided</scope>
</dependency>
------------------------------------------------

  They have to be both at least in <<<provided>>> scope.

  Then, you need to add following configuration into maven-war-plugin :

--------------------------------------------------------------------
<plugin>
  <artifactId>maven-war-plugin</artifactId>
  <version>${warPluginVersion}</version>
  <configuration>
    <archive>
      <manifest>
         <mainClass>org.nuiton.web.war.JettyLauncher</mainClass>
      </manifest>
    </archive>
    <overlays>
      <overlay>
        <groupId>org.nuiton.web</groupId>
        <artifactId>nuiton-web</artifactId>
        <type>jar</type>
        <includes>
          <include>**/war/Jetty*</include>
        </includes>
      </overlay>            
      <overlay>
        <groupId>org.eclispe.jetty</groupId>
        <artifactId>jetty-runner</artifactId>
        <type>jar</type>
      </overlay>
    </overlays>
  </configuration>
</plugin>
--------------------------------------------------------------------

** Extra configuration

  You can since version 1.13 configure the port and the contextPath when
  starting the executable war.

  To change the port, use the <<--port>> option and to change the
  contextPath use the <<--path>> option.

  Since version 1.19, you can add an extra option <<--start>> to specify the end of url of page to open in broswer.

  See the following example:

--------------------------------------------------------------------
java -jar mywar.war --port 8889 --path /context --start /index.html
--------------------------------------------------------------------

  The browser will open on page http://localhost:8889/context/index.html