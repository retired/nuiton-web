```diff
- Since 20180416 : This project as been retired due to inactivity and is no longer maintained
```

Objectif :
----------

Permettre de centraliser les ressources ou composants webs communs à
plusieurs applications.

Décomposition en modules :
--------------------------

- **Module pour tapestry** : nuiton-tapestry-extra
  Contient des composants/mixins/beans utiles aux applications Tapestry.
        En vrac : ZoneUpdater, AbstractMappedGridDataSource, Confirm, FeedBack,
FormatBinding, GenericSelectModel...

- **Module ressources** : nuiton-web-resource
  Contient des css communs et des icones.

Il serait peut-être intéressant d'avoir des modules séparés pour les css
et les icônes (ces dernières pouvant potentiellement être utilisées dans
une appli swing). 
