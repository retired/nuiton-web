/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 - 2013 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security;

/**
 * Security related utils method.
 * 
 * @author Eric Chatellier
 */
public class SecurityUtil {

    /**
     * Split url with separators definined in separator string.
     * 
     * @param url url to convert
     * @param separators each string character is used a separator char (null allowed)
     * @return shiro permission (: separator)
     */
    public static String convertToShiroPerm(String url, String separators) {
        String perm = url;
        if (separators != null) {
            for (int i = 0; i < separators.length(); i++) {
                perm = perm.replace(separators.charAt(i), ':');
            }
        }

        // cas de la permission url: qui est equivalente a url:*
        if (perm.endsWith(":")) {
            perm += "/";
        }
        
        return perm;
    }
}
