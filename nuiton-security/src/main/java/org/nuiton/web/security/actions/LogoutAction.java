/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security.actions;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.nuiton.web.security.SecurityUserTopiaDao;

public class LogoutAction extends AbstractAction {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    protected SecurityUserTopiaDao securityUserDAO;

    @Override
    public String execute() throws Exception {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();

        return SUCCESS;
    }
}
