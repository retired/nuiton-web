/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 - 2013 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Callable;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.topia.persistence.TopiaApplicationContextCache;
import org.nuiton.topia.persistence.TopiaConfigurationConstants;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.util.TopiaUtil;
import org.nuiton.web.SecurityEntityEnum;
import org.nuiton.web.SecurityTopiaApplicationContext;
import org.nuiton.web.SecurityTopiaPersistenceContext;

import com.google.common.base.Function;

/**
 * Servlet filter used to filter requested url.
 * 
 * @author Eric Chatellier
 */
public class SecurityShiroFilter extends AbstractShiroFilter {

    private static final Log log = LogFactory.getLog(SecurityShiroFilter.class);

    public static final String APP_CONFIG_CONTEXT = SecurityShiroFilter.class.getName() + "#" + ApplicationConfig.class.getName();

    public static final String ROOT_CONTEXT_CONTEXT = SecurityShiroFilter.class.getName() + "#" + SecurityTopiaApplicationContext.class.getName();

    public static final String ANON_LOGIN = "anonymous";

    public static final String SESSION_REQUESTED_URL = "securityRequestedUrl";
    protected static final Function<Properties,SecurityTopiaApplicationContext> CREATE_SECURITY_APPLICATION_CONTEXT = new Function<Properties, SecurityTopiaApplicationContext>() {
        @Override
        public SecurityTopiaApplicationContext apply(Properties input) {
            return new SecurityTopiaApplicationContext(input);
        }
    };

    protected ApplicationConfig config;

    protected SecurityTopiaApplicationContext rootContext;

    @Override
    public void init() throws Exception {
        
        // get config from context
        config = (ApplicationConfig)getServletContext().getAttribute(APP_CONFIG_CONTEXT);
        if (config == null) {
            throw new IllegalArgumentException("No APP_CONFIG_CONTEXT attribute found in servlet context");
        }

        // get topia root context
        Properties props = config.getFlatOptions();
        rootContext = TopiaApplicationContextCache.getContext(props, CREATE_SECURITY_APPLICATION_CONTEXT);
        initSchema(rootContext);
        getServletContext().setAttribute(ROOT_CONTEXT_CONTEXT, rootContext);
        
        // see http://shiro.apache.org/configuration.html#Configuration-ProgrammaticConfiguration
        if (log.isInfoEnabled()) {
            log.info("Overriding shiro realms");
        }

        Realm realm = new TopiaSecurityRealm(rootContext, config);
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager(realm);
        securityManager.setSubjectFactory(new SecuritySubjectFactory());
        securityManager.setSessionManager(new DefaultWebSessionManager());
        setSecurityManager(securityManager);
        SecurityUtils.setSecurityManager(securityManager);
    }

    protected static void initSchema(SecurityTopiaApplicationContext rootContext) throws TopiaException {
        SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
        SecurityUserTopiaDao securityUserDAO = transaction.getSecurityUserDao();
        if (securityUserDAO.count() == 0) {
            if (log.isInfoEnabled()) {
                log.info("Create anon user");
            }

            // create anon user
            SecurityUser anonUser = securityUserDAO.create();
            anonUser.setLogin(ANON_LOGIN);
            transaction.commit();
        } else {
            if (log.isDebugEnabled()) {
                log.debug("At least one user found, skip anon user creation");
            }
        }
        transaction.close();
    }

    @Override
    protected void doFilterInternal(ServletRequest servletRequest,
            ServletResponse servletResponse, final FilterChain chain)
            throws ServletException, IOException {

        // get subject
        Subject subjectUser = createSubject(servletRequest, servletResponse);
        if (log.isDebugEnabled()) {
            log.debug("Testing permission for user " + subjectUser.getPrincipal());
        }

        // to have permission, a user must be authenticated
        // can't use !subjectUser.isAuthenticated() here (for cache purpose)
        if (subjectUser.getPrincipal() == null) {
            subjectUser.login(new UsernamePasswordToken(ANON_LOGIN, ""));
        }

        // configuration
        String loginUrl = config.getOption("topia.security.loginurl");

        // get permission from requested uri without context path
        HttpServletRequest httpServletRequest = ((HttpServletRequest)servletRequest);
        String contextPath = httpServletRequest.getContextPath();
        String uri = httpServletRequest.getRequestURI();
        // removing jsession id from url
        uri = uri.replaceFirst(";(jsessionid|JSESSIONID)=[\\w-]*", "");
        if (uri.startsWith(contextPath)) {
            uri = uri.substring(contextPath.length());
        }
        String perm = SecurityUtil.convertToShiroPerm(uri, config.getOption("topia.security.separators"));

        if (subjectUser.isPermitted("url" + perm)) {
            if (log.isDebugEnabled()) {
                log.debug("User is permitted to access " + perm);
            }

            // on peu demander explicitement la page de login
            // dans ca cas, il faut sauvegarder la page d'avant comme url
            // de retour (seulement s'il n'y a pas deja une valeur de retour)
            Session session = subjectUser.getSession();
            if (uri.equals(loginUrl)) {
                // il ne faut pas ecraser l'url intercepter lors
                // de la redirection vers la page de login
                if (session.getAttribute(SESSION_REQUESTED_URL) == null) {
                    String referer = httpServletRequest.getHeader("referer");
                    if (referer != null) {
                        // removing jsession id from url
                        referer = referer.replaceFirst(";(jsessionid|JSESSIONID)=[\\w-]*", "");
                        if (!referer.endsWith(loginUrl)) {
                            if (log.isDebugEnabled()) {
                                log.debug("Remembering referer as " + referer);
                            }
                            session.setAttribute(SESSION_REQUESTED_URL, referer);
                        }
                    }
                }
            }

            // on devrait appeler simplement super.doFilterInternal(servletRequest, servletResponse, chain);
            // mais on ne peut pas car il recreer un nouveau Subject
            // et n'utilise pas le notre :(
            final ServletRequest request = prepareServletRequest(servletRequest, servletResponse, chain);
            final ServletResponse response = prepareServletResponse(request, servletResponse, chain);
            //noinspection unchecked
            subjectUser.execute(new Callable() {
                public Object call() throws Exception {
                    updateSessionLastAccessTime(request, response);
                    executeChain(request, response, chain);
                    return null;
                }
            });

        } else {
            if (log.isDebugEnabled()) {
                log.debug("User is NOT permitted to access " + perm);
            }
            if (subjectUser.isAuthenticated()) {
                ((HttpServletResponse)servletResponse).sendError(401, "Not authorized to access " + uri);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Redirecting user to login page");
                }
                // save request and redirect to login
                Session session = subjectUser.getSession();
                StringBuffer requestURL = new StringBuffer(httpServletRequest.getRequestURL());
                if (httpServletRequest.getQueryString() != null) {
                    requestURL.append('?').append(httpServletRequest.getQueryString());
                }
                session.setAttribute(SESSION_REQUESTED_URL, requestURL.toString());
                String redirect = contextPath + config.getOption("topia.security.loginurl");
                ((HttpServletResponse)servletResponse).sendRedirect(redirect);
            }
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        
        if (rootContext != null) {
            try {
                rootContext.close();
            } catch (TopiaException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't close root context", ex);
                }
            }
        }
    }
    
    
}
