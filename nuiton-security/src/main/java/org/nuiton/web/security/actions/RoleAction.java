/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security.actions;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.web.SecurityTopiaPersistenceContext;
import org.nuiton.web.security.SecurityRole;
import org.nuiton.web.security.SecurityRoleImpl;
import org.nuiton.web.security.SecurityRoleTopiaDao;

public class RoleAction extends AbstractAction {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(RoleAction.class);

    protected SecurityRoleTopiaDao securityRoleDAO;

    protected SecurityRole role;

    public SecurityRole getRole() {
        if (role == null) {
            String roleId = getParameter("roleId");
            if (StringUtils.isNotBlank(roleId)) {
                SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
                securityRoleDAO = transaction.getSecurityRoleDao();
                role = securityRoleDAO.findByTopiaId(roleId);
                transaction.close();
            } else {
                role = new SecurityRoleImpl();
            }
        }
        return role;
    }

    public String delete() throws Exception {
        try {
            String roleId = getParameter("roleId");
            if (StringUtils.isNotBlank(roleId)) {
                SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
                securityRoleDAO = transaction.getSecurityRoleDao();
                SecurityRole role = securityRoleDAO.findByTopiaId(roleId);
                securityRoleDAO.delete(role);
                transaction.commit();
                transaction.close();
            }
            
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't delete role", ex);
            }
        }

        return SUCCESS;
    }

    @Override
    public String execute() throws Exception {
        String result = super.execute();

        try {
            SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
            securityRoleDAO = transaction.getSecurityRoleDao();
            if (getRole().getTopiaId() == null) {
                securityRoleDAO.create(getRole());
            } else {
                securityRoleDAO.update(getRole());
            }
            transaction.commit();
            transaction.close();
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            result = input();
        }
        return result;
    }
}
