/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.nuiton.i18n.I18n;
import org.nuiton.web.security.SecurityShiroFilter;

public class LoginAction extends AbstractAction {

    private static final Log log = LogFactory.getLog(LoginAction.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    protected String login;

    protected String password;

    protected String savedUrl;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String execute() throws Exception {
        String result = SUCCESS;

        if (login == null || password == null) {
            result = input();
        } else {
            try {
                Subject currentUser = SecurityUtils.getSubject();
                UsernamePasswordToken token = new UsernamePasswordToken(login, password);
                currentUser.login(token);

                // marche pas :(
                //SavedRequest savedRequest = WebUtils.getAndClearSavedRequest(request);
                Session session = currentUser.getSession();
                savedUrl = (String)session.getAttribute(SecurityShiroFilter.SESSION_REQUESTED_URL);
                if (savedUrl != null) { // can be
                    if (log.isDebugEnabled()) {
                        log.debug("Redirecting to saved url " + savedUrl);
                    }
                    session.removeAttribute(SecurityShiroFilter.SESSION_REQUESTED_URL);
                    result = "redirect";
                } else {
                    result = SUCCESS;
                }
            } catch (UnknownAccountException ex) {
                addActionError(I18n.t("Identifiant ou mot de passe invalide !"));
                log.warn("Unknow user account", ex);
                result = input();
            } catch (IncorrectCredentialsException ex) {
                addActionError(I18n.t("Identifiant ou mot de passe invalide !"));
                log.warn("Invalid password", ex);
                result = input();
            } catch (LockedAccountException ex) {
                addActionError(I18n.t("Compte bloqué. Contacter un administrateur"));
                log.error("Account locked error", ex);
                result = input();
            } catch (ExcessiveAttemptsException ex) {
                addActionError(I18n.t("Nombre de tentatives dépassé"));
                log.error("Excessive attemps error", ex);
                result = input();
            } catch (AuthenticationException ex) {
                addActionError(ex.getMessage());
                log.warn("Authentication error", ex);
                result = input();
            }
        }
        return result;
    }

    public String getSavedUrl() {
        return savedUrl;
    }

}
