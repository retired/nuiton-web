/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security.actions;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.util.StringUtil;
import org.nuiton.web.SecurityTopiaPersistenceContext;
import org.nuiton.web.security.SecurityUser;
import org.nuiton.web.security.SecurityUserImpl;
import org.nuiton.web.security.SecurityUserTopiaDao;

public class UserAction extends AbstractAction {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(UserAction.class);

    protected SecurityUserTopiaDao securityUserDAO;

    protected SecurityUser user;

    protected String password;

    protected String confirm;

    public SecurityUser getUser() {
        if (user == null) {
            String roleId = getParameter("userId");
            if (StringUtils.isNotBlank(roleId)) {
                SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
                securityUserDAO = transaction.getSecurityUserDao();
                user = securityUserDAO.findByTopiaId(roleId);
                transaction.close();
            } else {
                user = new SecurityUserImpl();
            }
        }
        return user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String delete() throws Exception {
        try {
            String roleId = getParameter("userId");
            if (StringUtils.isNotBlank(roleId)) {
                SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
                securityUserDAO = transaction.getSecurityUserDao();
                SecurityUser user = securityUserDAO.findByTopiaId(roleId);
                securityUserDAO.delete(user);
                transaction.commit();
                transaction.close();
            }
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't delete user", ex);
            }
        }

        return SUCCESS;
    }

    @Override
    public void validate() {
        if (StringUtils.isNotBlank(password)) {
            if (!password.equals(confirm)) {
                addActionError(I18n.t("Les mots de passes ne sont pas identiques !"));
            }
        }
    }

    @Override
    public String execute() throws Exception {
        String result = super.execute();

        try {
            SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
            securityUserDAO = transaction.getSecurityUserDao();
            
            SecurityUser user = getUser();
            if (StringUtils.isNotBlank(password)) {
                String md5Password = StringUtil.encodeMD5(password);
                user.setPassword(md5Password);
            }
            if (user.getTopiaId() == null) {
                securityUserDAO.create(user);
            } else {
                securityUserDAO.update(user);
            }
            transaction.commit();
            transaction.close();
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            result = input();
        }
        return result;
    }
}
