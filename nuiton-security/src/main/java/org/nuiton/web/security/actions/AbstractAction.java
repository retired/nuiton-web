/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security.actions;

import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.util.ServletContextAware;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.web.SecurityTopiaApplicationContext;
import org.nuiton.web.security.SecurityShiroFilter;

import com.opensymphony.xwork2.ActionSupport;

public class AbstractAction extends ActionSupport implements ServletContextAware, ParameterAware {

    /** serialVersionUID. */
    private static final long serialVersionUID = -1097798007319592593L;

    protected SecurityTopiaApplicationContext rootContext;
    protected ApplicationConfig config;
    protected Map<String, String[]> actionParameters;

    @Override
    public void setParameters(Map<String, String[]> parameters) {
        this.actionParameters = parameters;
    }

    @Override
    public void setServletContext(ServletContext context) {
        config = (ApplicationConfig)context.getAttribute(SecurityShiroFilter.APP_CONFIG_CONTEXT);
        rootContext = (SecurityTopiaApplicationContext)context.getAttribute(SecurityShiroFilter.ROOT_CONTEXT_CONTEXT);
    }

    /**
     * Renvoie la valeur d'un paramètre de la request. Cette méthode peut
     * être appelée pour récupérer la valeur d'un paramètre avant que
     * l'intercepteur pousse les valeurs saisies dans un formulaire.
     *
     * @param parameterKey l'identifiant du paramètre
     * @return sa valeur
     */
    public String getParameter(String parameterKey) {
        String result = null;
        if (actionParameters != null) {
            String[] parameterValues = actionParameters.get(parameterKey);
            if (parameterValues != null && parameterValues.length >= 1) {
                result = parameterValues[0];
            }
        }
        return result;
    }
    
    /**
     * Renvoie la valeur d'un paramètre de la request. Cette méthode peut
     * être appelée pour récupérer la valeur d'un paramètre avant que
     * l'intercepteur pousse les valeurs saisies dans un formulaire.
     *
     * @param parameterKey l'identifiant du paramètre
     * @return sa valeur
     */
    public String[] getParameters(String parameterKey) {
        String[] result = null;
        if (actionParameters != null) {
            result = actionParameters.get(parameterKey);
        }
        return result;
    }
}
