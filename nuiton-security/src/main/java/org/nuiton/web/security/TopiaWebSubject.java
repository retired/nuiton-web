/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 - 2013 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.security;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.web.subject.support.WebDelegatingSubject;

/**
 * Classe redefinie pour surcharger la methode {@code isAuthenticated()}
 * car la methode login() la passe obligatoirement a {@code true}.
 * 
 * @author Eric Chatellier
 */
public class TopiaWebSubject extends WebDelegatingSubject {

    public TopiaWebSubject(PrincipalCollection principals, boolean authenticated,
            String host, Session session, boolean sessionEnabled,
            ServletRequest request, ServletResponse response,
            SecurityManager securityManager) {
        super(principals, authenticated, host, session, sessionEnabled, request, response, securityManager);
    }

    @Override
    public boolean isAuthenticated() {
        boolean result = super.isAuthenticated() && !SecurityShiroFilter.ANON_LOGIN.equals(principals.getPrimaryPrincipal());
        return result;
    }
}
