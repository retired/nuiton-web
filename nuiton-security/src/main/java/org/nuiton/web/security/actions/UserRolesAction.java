/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security.actions;

import java.util.List;

import org.nuiton.web.SecurityTopiaPersistenceContext;
import org.nuiton.web.security.SecurityRole;
import org.nuiton.web.security.SecurityRoleTopiaDao;
import org.nuiton.web.security.SecurityUser;
import org.nuiton.web.security.SecurityUserTopiaDao;

public class UserRolesAction extends AbstractAction {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    protected SecurityUserTopiaDao securityUserDAO;

    protected SecurityRoleTopiaDao securityRoleDAO;

    protected List<SecurityUser> users;

    protected List<SecurityRole> roles;

    protected List<String> userIds;

    @Override
    public String input() throws Exception {
        SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
        securityUserDAO = transaction.getSecurityUserDao();
        securityRoleDAO = transaction.getSecurityRoleDao();
        users = securityUserDAO.newQueryBuilder().setOrderByArguments(SecurityUser.PROPERTY_LOGIN).findAll();
        roles = securityRoleDAO.newQueryBuilder().setOrderByArguments(SecurityRole.PROPERTY_NAME).findAll();
        transaction.close();
        return super.input();
    }

    public List<SecurityUser> getUsers() {
        return users;
    }

    public List<SecurityRole> getRoles() {
        return roles;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    @Override
    public String execute() throws Exception {
        String result = super.execute();

        if (userIds == null) {
            result = input();
        } else {
            try {
                SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
                securityUserDAO = transaction.getSecurityUserDao();
                securityRoleDAO = transaction.getSecurityRoleDao();
                for (String userId : userIds) {
                    SecurityUser securityUser = securityUserDAO.findByTopiaId(userId);
                    securityUser.clearSecurityRole();

                    String[] roleIds = getParameters("roles-" + userId);
                    if (roleIds != null) {
                        for (String roleId : roleIds) {
                            SecurityRole secuRole = securityRoleDAO.findByTopiaId(roleId);
                            securityUser.addSecurityRole(secuRole);
                        }
                    }
                }
                transaction.commit();
                transaction.close();
            } catch (Exception ex) {
                addActionError(ex.getMessage());
                result = input();
            }
        }
        return result;
    }
}
