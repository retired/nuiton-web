/*
 * #%L
 * Nuiton Web :: Nuiton Security
 * %%
 * Copyright (C) 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.security.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.web.SecurityTopiaPersistenceContext;
import org.nuiton.web.security.SecurityRole;
import org.nuiton.web.security.SecurityRoleTopiaDao;
import org.nuiton.web.security.SecurityUserTopiaDao;

public class RolePermissionsAction extends AbstractAction {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    protected SecurityUserTopiaDao securityUserDAO;

    protected SecurityRoleTopiaDao securityRoleDAO;

    /** Id categories with name. */
    protected Map<String, String> categories;
    /** Id permission with name. */
    protected Map<String, String> permissions;
    /** Id categories with permissions ids. */
    protected Map<String, Collection<String>> categoryPermissions;

    protected List<SecurityRole> roles;

    protected List<String> roleIds;

    @Override
    public String input() throws Exception {
        SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
        securityRoleDAO = transaction.getSecurityRoleDao();
        roles = securityRoleDAO.newQueryBuilder().setOrderByArguments(SecurityRole.PROPERTY_NAME).findAll();
        transaction.close();
        
        categories = new HashMap<String, String>();
        permissions = new HashMap<String, String>();
        categoryPermissions = new HashMap<String, Collection<String>>();

        Properties props = config.getFlatOptions();
        for (String prop : props.stringPropertyNames()) {
            if (prop.startsWith("topia.security.permission.")) {
                String endProp = StringUtils.removeStart(prop, "topia.security.permission.");
                String[] subs = endProp.split("\\.");
                if (subs.length == 1) {
                    categories.put(subs[0], props.getProperty(prop));
                } else if (subs.length == 3) {
                    if (subs[2].equals("name")) {
                        // name
                        String name = props.getProperty(prop);
                        permissions.put(subs[1], name);
                        // association
                        Collection<String> categoryPermissionCol = categoryPermissions.get(subs[0]);
                        if (categoryPermissionCol == null) {
                            categoryPermissionCol = new ArrayList<String>();
                            categoryPermissions.put(subs[0], categoryPermissionCol);
                        }
                        categoryPermissionCol.add(subs[1]);
                    }
                }
            }
        }
        return super.input();
    }

    public List<SecurityRole> getRoles() {
        return roles;
    }

    public Map<String, String> getCategories() {
        return categories;
    }

    public Map<String, String> getPermissions() {
        return permissions;
    }

    public Map<String, Collection<String>> getCategoryPermissions() {
        return categoryPermissions;
    }

    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    @Override
    public String execute() throws Exception {
        String result = super.execute();

        if (roleIds == null) {
            result = input();
        } else {
            try {
                SecurityTopiaPersistenceContext transaction = rootContext.newPersistenceContext();
                securityRoleDAO = transaction.getSecurityRoleDao();
                for (String roleId : roleIds) {
                    SecurityRole securityRole = securityRoleDAO.findByTopiaId(roleId);
                    securityRole.clearPermissions();

                    String[] permissions = getParameters("permissions-" + roleId);
                    if (permissions != null) {
                        for (String permission : permissions) {
                            securityRole.addPermissions(permission);
                        }
                    }
                }
                transaction.commit();
            } catch (Exception ex) {
                addActionError(ex.getMessage());
                result = input();
            }
        }
        return result;
    }
}
