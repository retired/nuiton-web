<%--
  #%L
  Nuiton Web :: Nuiton Security
  %%
  Copyright (C) 2012 - 2013 CodeLutin, Chatellier Eric
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Sécurité</title>
 </head>

 <body>

  <div class="security-div">

    <h1>Sécurité</h1>
  
    <h2>Permissions des rôles</h2>
    
    <form action="<s:url action="role-permissions" namespace="/security" />" method="post" cssClass="security-form">
      <s:iterator value="roles">
        <input type="hidden" name="roleIds" value="<s:property value="topiaId" />" />
      </s:iterator>
      <table class="security-table">
       <s:iterator value="categories.keys" var="category">
         <s:set var="categoryName" value="categories.get(#category)" />
         <tr>
          <th colspan="<s:property value="roles.size() + 1" />"><s:property value="#categoryName" /></th>
         </tr>

         <tr class="security-roles">
          <td class="security-empty" />
          <s:iterator value="roles">
           <td><a href="<s:url action='role!input' namespace='/security'>
                  <s:param name="roleId"><s:property value="topiaId" /></s:param>
                </s:url>">
                  <s:property value="name" />
                </a></td>
          </s:iterator>
         </tr>

         <s:iterator value="categoryPermissions.get(#category)" var="categoryPermission">
         <s:set var="permissionName" value="permissions.get(#categoryPermission)" />
          <tr>
           <td><s:property value="#permissionName" /></td>
           <s:iterator value="roles" var="role">
             <td><input type="checkbox" name="permissions-<s:property value="topiaId" />"
               value="<s:property value="#categoryPermission"/>"
               <s:if test="#role.permissions.contains(#categoryPermission)" >
                    checked="checked"
               </s:if> /></td>
           </s:iterator>
          </tr>
         </s:iterator>
       </s:iterator>
      </table>
      <input type="submit" value="Valider" />
    </form>
  
    <h2>Gestion</h2>
  
    <div class="secu-newuser">
      <a href="<s:url action='user-roles!input' namespace='/security' />">Rôles des utilisateurs</a>
    </div>
    <div class="secu-newuser">
      <a href="<s:url action='user!input' namespace='/security' />">Nouvel utilisateur</a>
    </div>
    <div class="secu-newrole">
      <a href="<s:url action='role!input' namespace='/security' />">Nouveau r&ocirc;le</a>
    </div>
  </div>

 </body>
</html>
