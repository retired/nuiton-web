<%--
  #%L
  Nuiton Web :: Nuiton Security
  %%
  Copyright (C) 2012 - 2013 CodeLutin, Chatellier Eric
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Sécurité</title>
 </head>

 <body>
 
  <div class="security-div">
    <h1>Sécurité</h1>

    <s:if test='%{user.topiaId != null}'>
      <h2>Modification d'un utilisateur</h2>
    </s:if>
    <s:else>
      <h2>Nouveau utilisateur</h2>
    </s:else>

    <s:form action="user" namespace="/security" cssClass="security-form">
      <s:actionerror />
      <s:hidden name="userId" value="%{user.topiaId}" />
      <s:textfield label="Identifiant" name="user.login" value="%{user.login}"/>
      <s:password label="Mot de passe" name="password" />
      <s:password label="Confirmation" name="confirm" />
      <s:submit label="Valider" />
    </s:form>
    
    <s:if test='%{user.topiaId != null}'>
      <s:a action="user!delete" namespace="/security" cssClass="delete">
        <s:param name="userId"><s:property value="user.topiaId" /></s:param>
        Supprimer
      </s:a>
    </s:if>
  </div>
 </body>
</html>
