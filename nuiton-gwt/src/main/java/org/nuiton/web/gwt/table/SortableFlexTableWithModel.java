/*
 * #%L
 * Nuiton Web :: Nuiton GWT
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.gwt.table;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author jcouteau
 * @since 1.1
 */
public class SortableFlexTableWithModel extends FlexTable
        implements TableModelListener, ClickHandler {

    SortableTableModel model;
    
    //number of rows for update
    int rows = 0;

    public SortableFlexTableWithModel() {
        super();
        this.addClickHandler(this);
    }

    public SortableFlexTableWithModel(SortableTableModel model) {
        this();
        setModel(model);
    }

    public void setModel(SortableTableModel model) {

        removeAllRows();

        if (this.model != null) {
            this.model.removeTableModelListener(this);
        }
        this.model = model;
        getModel().addTableModelListener(this);
    }

    public SortableTableModel getModel() {
        return model;
    }

    @Override
    public void tableChanged(TableModelEvent event) {

        int firstColumn = event.getColumn();
        int lastColumn = event.getColumn();
        int type = event.getType();
        
        int oldRows = rows;
        
        if (firstColumn == TableModelEvent.ALL_COLUMNS) {
            firstColumn = 0;
            lastColumn = getColumnCount() - 1;
        }

        int firstRow = event.getFirstRow();
        int lastRow = event.getLastRow();

        if (type == TableModelEvent.DELETE) {
            for (int i = firstRow; i <= lastRow; i++) {
                removeRow(i + 1);
            }
        } else if (firstColumn == lastColumn) {
            if (firstRow == lastRow) {
                Object value = model.getValueAt(firstColumn, firstRow);
                setValueAt(value, firstRow, firstColumn);
            } else {
                for (int j = firstRow; j <= lastRow; j++) {
                    Object value = model.getValueAt(firstColumn, j);
                    setValueAt(value, j, firstColumn);
                }
            }
        } else {
            for (int i = firstColumn; i <= lastColumn; i++) {

                if (firstRow == lastRow) {
                    Object value = model.getValueAt(i, firstRow);
                    setValueAt(value, firstRow, i);
                    rows = 1;
                } else {
                    for (int j = firstRow; j < lastRow; j++) {
                        if (j < model.getRowCount()) {
                            Object value = model.getValueAt(i, j);
                            setValueAt(value, j, i);
                            rows = j+1;
                        } else {
                            break;
                        }
                    }
                }
                
                
            }
            
            for (int i = rows; i < oldRows; i++) {
                //remove all rows that are not in model anymore
                if ((rows+1) < getRowCount()){
                    removeRow(rows+1);
                }
            }
            
        }
    }

    protected void setValueAt(Object value, int row, int col) {
        if (value instanceof Widget) {
            setWidget(row + 1, col, (Widget) value);
        } else if (value == null) {
            setHTML(row + 1, col, "");
        } else {
            setHTML(row + 1, col, value.toString());
        }
    }

    protected int getColumnCount() {
        return model.getColumnCount();
    }

    @Override
    public void onClick(ClickEvent event) {
        int columnClicked = getCellForEvent(event).getCellIndex();
        int rowIndex = getCellForEvent(event).getRowIndex();

        int sortedColumn = model.getSortedColumn();

        int sortingOrder = model.getSortingOrder();

        if (rowIndex == 0) {
            if (sortedColumn == columnClicked) {
                switch (sortingOrder) {
                    case SortableTableModel.ASC:
                        model.setSortedColumn(columnClicked,
                                SortableTableModel.DSC);
                        getCellFormatter().setStyleName(0,columnClicked,"sortedUp");
                        break;
                    case SortableTableModel.DSC:
                        model.setSortedColumn(columnClicked,
                                SortableTableModel.ASC);
                        getCellFormatter().setStyleName(0,columnClicked,"sortedDown");
                        break;
                    default:
                        model.setSortedColumn(columnClicked,
                                SortableTableModel.ASC);
                        getCellFormatter().setStyleName(0,columnClicked,"sortedDown");
                }

            } else {
                model.setSortedColumn(columnClicked, SortableTableModel.ASC);
                if (sortedColumn>=0) {
                    getCellFormatter().setStyleName(0,sortedColumn,"sortable");
                }
                getCellFormatter().setStyleName(0,columnClicked,"sortedDown");
            }
        }
    }

    public void setColumnVisible(int col, boolean b) {
        for (int i = 0; i < getRowCount(); i++) {
            getCellFormatter().setVisible(i, col, b);
        }
    }
}
