/*
 * #%L
 * Nuiton Web :: Nuiton GWT
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.gwt.misc;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Label;
import org.nuiton.web.gwt.messages.UIMessages;

/**
 *
 * Waiting screen that blocks the user so that he can wait during long
 * operations.
 *
 * To display it, use :
 * <code>
 *     LoaderNotification.openLoaderPanel();
 * </code>
 *
 * To remove it, use :
 * <code>
 *     LoaderNotification.cloaseLoader();
 * </code>
 *
 * To style it, use the CSS class : <code>loading</code>
 *
 * @author jcouteau
 * @since 1.1
 */
public class LoaderNotification {

    protected static BlindedPopup glassPanel = null;

    public static void openLoaderPanel() {

        UIMessages messages = GWT.create(UIMessages.class);

        Label label = new Label(messages.loading());
        label.setStyleName("loading");
        glassPanel = new BlindedPopup(label);
    }

    public static void closeLoader() {
        if (glassPanel != null) {
            glassPanel.hide();
        }
    }
}
