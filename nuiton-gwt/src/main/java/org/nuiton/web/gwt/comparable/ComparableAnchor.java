/*
 * #%L
 * Nuiton Web :: Nuiton GWT
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.gwt.comparable;

import com.google.gwt.user.client.ui.Anchor;

/**
 *
 * Anchor that implements Comparable to be used in SortableTables. Comparison is
 * made on the text of the anchor.
 *
 * @author jcouteau
 * @since 1.1
 */
public class ComparableAnchor extends Anchor implements Comparable<Anchor> {

    public ComparableAnchor(String value){
        super(value);
    }

    @Override
    public int compareTo(Anchor o) {
        String oValue = o.getText();
        String value = getText();

        return value.compareTo(oValue);
    }
}
