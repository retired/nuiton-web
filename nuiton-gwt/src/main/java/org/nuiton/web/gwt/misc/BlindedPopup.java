/*
 * #%L
 * Nuiton Web :: Nuiton GWT
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.gwt.misc;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * This class provides a popup with a glasspanel behind so that users can't
 * interact with things that are behind.
 *
 * Use it like that :
 *
 * <code>
 * new BlindedPopup(myPopupContent);
 * </code>
 *
 * If you want to hide your BlindedPopup from the popup content use :
 *
 * <code>
 * ((BlindedPopup)this.getParent()).hide();
 * </code>
 *
 * So that the BlindedPopup works completely, you must have a 'wrapper' div
 * that is 100% width and height and the your website content inside :
 *
 * <code>
 * &lt;body&gt;
 *
 *   &lt;div id="wrapper"&gt;
 *     &lt;!-- Site content --&gt;
 *   &lt;/div&gt;
 *
 * &lt;/body&gt;
 * </code>
 *
 * <code>
 * #wrapper {
 *     height                      :    100%;
 *     width                       :    100%;
 *     overflow                    :    scroll;
 *     position:relative;
 * }
 * </code>
 *
 * @author jcouteau
 * @since 1.1
 */
public class BlindedPopup extends PopupPanel implements ClickHandler {
    PopupPanel glass;

    public BlindedPopup(Widget widget) {
        //init popup
        super(false);

        /*********************************************
         * A glass panel or 'blinder'
         * to wash out the current screen
         ********************************************/
        glass = new PopupPanel();
        glass.setStyleName("rx-glass");
        /*
         * Set full screen
         */
        DOM.setStyleAttribute(glass.getElement(), "width", "100%");
        DOM.setStyleAttribute(glass.getElement(), "height", "100%");
        /*
         * Add default styles if required
         */
        DOM.setStyleAttribute(glass.getElement(),
                    "backgroundColor", "#000");
        DOM.setStyleAttribute(glass.getElement(),
                    "opacity", "0.70");
//            DOM.setStyleAttribute(glass.getElement(),
//                    "-moz-opacity", "0.70");
        DOM.setStyleAttribute(glass.getElement(),
                    "filter", " alpha(opacity=70)");
        /**********************************************
         * A popup
         *********************************************/
        setStyleName("rx-BlindedPopup");

        setWidget(widget);

        /*
         * Show the glass first, then the popup will be over it
         */
        glass.show();
        center();
    }

    @Override
    public void onClick(ClickEvent event) {
        hide();
    }

    public void hide() {
        glass.hide();
        super.hide();
    }
}
