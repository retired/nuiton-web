/*
 * Copyright 1997-2001 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 *
 * %%Ignore-License
 */

package org.nuiton.web.gwt.table;

import java.util.EventObject;

/**
 *
 * Copy of javax.swing.event.TableModelEvent
 *
 * @author jcouteau
 * @since 1.1
 */
public class TableModelEvent extends EventObject {

    /** Identifies the addtion of new rows or columns. */
    public static final int INSERT = 1;

    /** Identifies a change to existing data. */
    public static final int UPDATE = 0;

    /** Identifies the removal of rows or columns. */
    public static final int DELETE = -1;

    /** Identifies the header row. */
    public static final int HEADER_ROW = -1;

    /** Specifies all columns in a row or rows. */
    public static final int ALL_COLUMNS = -1;

    private static final long serialVersionUID = 1L;

    protected int column;
    protected int firstRow;
    protected int lastRow;
    protected int type;

    public TableModelEvent(GWTTableModel source) {
        this(source, 0, Integer.MAX_VALUE, ALL_COLUMNS, UPDATE);
    }

    public TableModelEvent(GWTTableModel source, int row) {
        this(source, row, row, ALL_COLUMNS, UPDATE);
    }

    public TableModelEvent(GWTTableModel source, int firstRow, int lastRow) {
        this(source, firstRow, lastRow, ALL_COLUMNS, UPDATE);
    }

    public TableModelEvent(GWTTableModel source, int firstRow, int lastRow, int column) {
        this(source, firstRow, lastRow, column, UPDATE);
    }

    public TableModelEvent(GWTTableModel source, int firstRow, int lastRow, int column, int type) {
        super(source);
        this.firstRow = firstRow;
        this.lastRow = lastRow;
        this.column = column;
        this.type = type;
    }

    public int getColumn() {
        return column;
    }

    public int getFirstRow(){
        return firstRow;
    }

    public int getLastRow(){
        return lastRow;
    }

    public int getType() {
        return type;
    }

}
