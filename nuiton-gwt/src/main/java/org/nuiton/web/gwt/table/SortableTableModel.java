/*
 * #%L
 * Nuiton Web :: Nuiton GWT
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.gwt.table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class is used to provide sorting functionnalities when clicking on table
 * columns. It is based on an underlying model that contains data.
 *
 * @author jcouteau
 * @since 1.1
 */
public class SortableTableModel extends AbstractGWTTableModel
        implements Serializable , TableModelListener {

    private static final long serialVersionUID = 1L;

    protected GWTTableModel model;

    /** The sorted column, -1 means no sorting **/
    protected int sortedColumn = -1;

    /** The sorting order, -1 means no sorting **/
    protected int sortingOrder = -1;

    public final static int NO_SORTING = -1;

    public final static int ASC = 0;

    public final static int DSC = 1;

    protected int[] rowsOrder;

    public SortableTableModel(GWTTableModel model) {
        super();
        this.model = model;

        //listen to the underlying model changes
        model.addTableModelListener(this);

        initRowsOrder();
    }


    @Override
    public void tableChanged(TableModelEvent e) {
        initRowsOrder();
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return model.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return model.getColumnCount();
    }

    @Override
    public Object getValueAt(int col, int row) {
        
        int modelRow = 0;
        
        if (rowsOrder.length > 0) {
            modelRow = rowsOrder[row];
        }
        return model.getValueAt(col, modelRow);
    }

    public void setSortedColumn(int column) {
        setSortedColumn(column, ASC);
    }

    public void setSortedColumn(int column, int order) {
        int rowCount = model.getRowCount();

        if (isSortable(column)) {

            sortedColumn = column;
            sortingOrder = order;

            List originalColumn = new ArrayList();

            for (int i=0; i<rowCount; i++) {
                if (model.getValueAt(column,i) == null) {
                    return;
                }
                originalColumn.add(model.getValueAt(column, i));
            }

            List sortedColumn = new ArrayList(originalColumn);

            switch (order) {
                case ASC :
                    Collections.sort(sortedColumn);
                    break;
                case DSC :
                    Collections.sort(sortedColumn, Collections.reverseOrder());
                    break;
                default:
                    break;
            }

            for (int i=0;i<rowCount; i++) {
                Object value = sortedColumn.get(i);
                int originalRow = originalColumn.indexOf(value);
                originalColumn.set(originalRow,null);
                rowsOrder[i] = originalRow;
            }

            fireTableDataChanged();
        }
    }

    public int getSortedColumn() {
        return sortedColumn;
    }

    public void setSortingOrder(int order) {
        setSortedColumn(sortedColumn, order);
    }

    public int getSortingOrder() {
        return sortingOrder;
    }

    protected boolean isSortable(int column) {

        try {
            Comparable value = (Comparable)getValueAt(column, 0);
        } catch (ClassCastException eee) {
            return false;
        }
        return true;
    }

    protected void initRowsOrder() {

        int rowCount = model.getRowCount();
        rowsOrder = new int[rowCount];
        for (int i = 0;i<rowCount;i++) {
            rowsOrder[i] = i;
        }
    }
}
