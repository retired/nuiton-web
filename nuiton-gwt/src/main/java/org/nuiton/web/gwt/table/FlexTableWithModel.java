/*
 * #%L
 * Nuiton Web :: Nuiton GWT
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin, Jean Couteau
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.web.gwt.table;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author jcouteau
 * @since 1.1
 */
public class FlexTableWithModel extends FlexTable
        implements TableModelListener {

    GWTTableModel model;

    public FlexTableWithModel() {
        super();
    }

    public FlexTableWithModel(GWTTableModel model) {
        super();
        setModel(model);
    }

    public void setModel(GWTTableModel model) {

        removeAllRows();

        if (this.model != null){
            this.model.removeTableModelListener(this);
        }
        this.model = model;
        this.model.addTableModelListener(this);
    }

    public GWTTableModel getModel(){
        return model;
    }

    @Override
    public void tableChanged(TableModelEvent event) {

        int firstColumn = event.getColumn();
        int lastColumn = event.getColumn();
        int type = event.getType();


        if (firstColumn == TableModelEvent.ALL_COLUMNS) {
            firstColumn = 0;
            lastColumn = getColumnCount() - 1;
        }

        int firstRow = event.getFirstRow();
        int lastRow = event.getLastRow();

        if (type == TableModelEvent.DELETE) {
            for (int i = firstRow; i<=lastRow; i++) {
                removeRow(i+1);
            }
        } else if (firstColumn == lastColumn) {
            if (firstRow == lastRow) {
                Object value = model.getValueAt(firstColumn, firstRow);
                setValueAt(value, firstRow, firstColumn);
            } else {
                for (int j = firstRow; j <= lastRow; j++) {
                    Object value = model.getValueAt(firstColumn, j);
                    setValueAt(value, j, firstColumn);
                }
            }
        } else {
            for (int i=firstColumn; i<lastColumn; i++) {

                if (firstRow == lastRow) {
                    Object value = model.getValueAt(i, firstRow);
                    setValueAt(value,firstRow,i);
                } else {
                    for (int j=firstRow; j<=lastRow; j++) {
                        Object value = model.getValueAt(i, j);
                        setValueAt(value,j,i);
                    }
                }
            }
        }
    }

    protected void setValueAt(Object value, int row, int col) {
        if (value instanceof Widget) {
            setWidget(row+1, col, (Widget) value);
        } else if (value == null) {
            setHTML(row+1, col, "");
        } else {
            setHTML(row+1, col, value.toString());
        }
    }

    protected int getColumnCount() {
        return model.getColumnCount();
    }
}
