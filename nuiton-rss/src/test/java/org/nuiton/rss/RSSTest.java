/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import org.apache.commons.beanutils.BeanUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author poussin
 */
public class RSSTest {

    @Test
    public void testBeanUtils() throws Exception {
//        RSSConfig.class.getField("TIME_FORCE_RETRIEVED").set(null, 10);
        System.out.println("TIME:" + RSSConfig.TIME_FORCE_RETRIEVED);
        System.out.println("RENDER:" + RSSConfig.DEFAULT_RENDERER_CLASS);
        BeanUtils.setProperty(new RSSConfig(), "TIME_FORCE_RETRIEVED", "10");
        BeanUtils.setProperty(new RSSConfig(), "DEFAULT_RENDERER_CLASS", FeedRenderer.class.getName());
        System.out.println("TIME:" + RSSConfig.TIME_FORCE_RETRIEVED);
        System.out.println("RENDER:" + RSSConfig.DEFAULT_RENDERER_CLASS);
        Assert.assertEquals("java.lang.String", BeanUtils.getProperty("Object", "class.name"));
    }

    @Test
    public void testCleaner() throws Exception {
        String s = "debut<Script src='toto.js'>et du script</scRipt>milieu<scripT>encore du code\n</script>fin";

        HTMLCleaner cleaner = new HTMLScriptCleaner();
        Assert.assertEquals("debutmilieufin", cleaner.clean(s));
    }

    @Test
    public void testRendererConfig() throws Exception {
        FeedRendererConfig c = new FeedRendererConfig("indat-ndat");

        List<Field> l1 = new ArrayList<Field>();
        l1.add(Field.IMAGE);
        l1.add(Field.NAME);
        l1.add(Field.DESCRIPTION);
        l1.add(Field.AUTHOR);
        l1.add(Field.TIME);

        List<Field> l2 = new ArrayList<Field>();
        l2.add(Field.NAME);
        l2.add(Field.DESCRIPTION);
        l2.add(Field.AUTHOR);
        l2.add(Field.TIME);

        Assert.assertEquals(l1, c.getFeedFieldOrder());
        Assert.assertEquals(l2, c.getItemFieldOrder());
        Assert.assertEquals("indat-ndat", c.getString());
    }

    /**
     * Test of getHTML method, of class RSSHelper.
     * TODO do the test, for the moment, do not launch it...
     *
     * @throws Exception if any pb
     */
    @Ignore
    public void testGetHTML() throws Exception {
        System.out.println("getHTML");
        String feedName = "http://localhost/rss/rss_1.0.xml";
        String feedRepr = "n-ndat";
        int nbItem = -1;
        RSSHelper instance = new RSSHelper();
        String expResult = "";
        String result = instance.getHTML(feedName, feedRepr, nbItem, false);
        System.out.println("***" + result);
        result = instance.getHTML(feedName, feedRepr, nbItem, false);
        System.out.println("***" + result);

        Thread.sleep(2000);

        result = instance.getHTML(feedName, feedRepr, nbItem, false);
        System.out.println("***" + result);
        result = instance.getHTML(feedName, feedRepr, nbItem, false);
        System.out.println("***" + result);
        //        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

}
