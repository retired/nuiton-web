/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** @author tony */
public class RSSGeneratorHelperTest {

    private static Log log = LogFactory.getLog(RSSGeneratorHelperTest.class);

    private static final long TIMESTAMP = System.nanoTime();

    RSSGenerator helper;

    File f;

    @Rule
    public TestName testMethodName = new TestName();

    @Before
    public void setUp() throws Exception {
        String base = System.getProperty("java.io.tmpdir");
        if (base == null || base.isEmpty()) {
            base = new File("").getAbsolutePath();
        }
        File testBasedir = new File(base);
        if (log.isInfoEnabled()) {
            log.info("basedir for test " + testBasedir);
        }
        File testdir = FileUtil.getFileFromFQN(testBasedir,
                                               getClass().getName() + "." + TIMESTAMP);

        helper = RSSGenerator.newDefaultInstance(testdir);


        f = getFeedFile();

    }

    @Test
    public void testCreateFeed() throws Exception {

        Map<Field, Object> values = new HashMap<Field, Object>();
        values.put(Field.NAME, "feedName : " + f.getName());
        values.put(Field.DESCRIPTION, "feedDescription : " + f.getName());
        values.put(Field.LINK, f.toURI().toURL() + "");
        values.put(Field.AUTHOR, "feed author");
        values.put(Field.CATEGORY, Arrays.asList("categorie1", "categorie2"));
        values.put(Field.TIME, RSSGenerator.DATE_PARSER.format(new Date()));

        Assert.assertFalse(f.exists());
        if (log.isInfoEnabled()) {
            log.info("feedFile : " + f);
        }
        helper.createFeedFile(f.toURI().toURL(), helper.getFormat(), values);
        // no lock
        Assert.assertFalse(helper.getWriteFileLock(f).exists());
        // file exist
        Assert.assertTrue(f.exists());

        SyndFeed feed = RSSIOUtil.readFeed(f.toURI().toURL());

        if (log.isDebugEnabled()) {
            log.debug(feed);
        }

        Assert.assertEquals(feed.getFeedType(), helper.getFormat().getType());
        Assert.assertEquals(feed.getLink(), values.get(Field.LINK));
        Assert.assertEquals(feed.getTitle(), values.get(Field.NAME));
        Assert.assertEquals(feed.getDescription(), values.get(Field.DESCRIPTION));
        Assert.assertEquals(feed.getAuthor(), values.get(Field.AUTHOR));
        List<?> cats = (List<?>) values.get(Field.CATEGORY);
        for (Object o : feed.getCategories()) {
            SyndCategory cat = (SyndCategory) o;
            Assert.assertTrue(cats.contains(cat.getName()));
        }
        Assert.assertEquals(feed.getPublishedDate(), RSSGenerator.DATE_PARSER.parse((String) values.get(Field.TIME)));

    }

    @Test
    public void testCreateFeedAlreadyExisting() throws Exception {

        testCreateFeed();

        Map<Field, Object> values = new HashMap<Field, Object>();
        values.put(Field.NAME, "feedName : " + f.getName());
        values.put(Field.DESCRIPTION, "feedDescription : " + f.getName());
        values.put(Field.LINK, f.toURI().toURL() + "");
        values.put(Field.TIME, RSSGenerator.DATE_PARSER.format(new Date()));

        Assert.assertTrue(f.exists());

        try {
            helper.createFeedFile(f.toURI().toURL(), helper.getFormat(), values);
            // file already exist, can not reach this code
            Assert.fail();
        } catch (IllegalStateException e) {
            Assert.assertTrue(true);
        }
        // no write lock
        Assert.assertFalse(helper.getWriteFileLock(f).exists());

    }

    @Test
    public void testAddFeedEntry() throws Exception {

        testCreateFeed();

        Assert.assertTrue(f.exists());
        Map<Field, Object> values = createEntry(0);

        helper.addItemToFeedFile(f.toURI().toURL(), 2, values);

        SyndFeed feed = RSSIOUtil.readFeed(f.toURI().toURL());

        List entries = feed.getEntries();
        Assert.assertEquals(1, entries.size());

        if (log.isDebugEnabled()) {
            log.debug(feed);
        }

        SyndEntry entry = (SyndEntry) entries.get(0);

        assertEntry(entry, values);
    }

    @Test
    public void testAddFeedEntryToMax() throws Exception {

        testAddFeedEntry();

        Assert.assertTrue(f.exists());
        SyndFeed feed = RSSIOUtil.readFeed(f.toURI().toURL());

        List entries = feed.getEntries();
        Assert.assertEquals(1, entries.size());
        SyndEntry firsEntry = (SyndEntry) entries.get(0);

        Map<Field, Object> values;

        int nbMaxEntries = 10;

        for (int i = 1; i < nbMaxEntries; i++) {
            values = createEntry(i);
            helper.addItemToFeedFile(f.toURI().toURL(), nbMaxEntries, values);
            feed = RSSIOUtil.readFeed(f.toURI().toURL());

            entries = feed.getEntries();
            Assert.assertEquals(i + 1, entries.size());

            SyndEntry entry = (SyndEntry) entries.get(i);

            assertEntry(entry, values);
        }

        // feed file contains max items

        values = createEntry(nbMaxEntries);

        helper.addItemToFeedFile(f.toURI().toURL(), nbMaxEntries, values);

        feed = RSSIOUtil.readFeed(f.toURI().toURL());

        entries = feed.getEntries();

        Assert.assertEquals(nbMaxEntries, entries.size());

        SyndEntry entry = (SyndEntry) entries.get(nbMaxEntries - 1);

        assertEntry(entry, values);

        // check orginal first entry is no more present
        entry = (SyndEntry) entries.get(0);
        Assert.assertFalse(firsEntry.getTitle().equals(entry.getTitle()));
    }

    @Test
    public void testCleanFile() throws Exception {
        testCreateFeed();
        try {
            Assert.assertNotNull(f);
            Assert.assertTrue(f.exists());
            // lock is delete
            Assert.assertFalse(helper.getWriteFileLock(f).exists());
        } finally {
            if (f != null) {
                f.deleteOnExit();
            }
        }
    }

    protected void assertEntry(SyndEntry entry, Map<Field, Object> values) throws ParseException {
        Assert.assertEquals(entry.getLink(), values.get(Field.LINK));
        Assert.assertEquals(entry.getTitle(), values.get(Field.NAME));
        Assert.assertEquals(entry.getDescription().getValue(), values.get(Field.DESCRIPTION));
        Assert.assertEquals(entry.getAuthor(), values.get(Field.AUTHOR));
        Assert.assertEquals(entry.getPublishedDate(), RSSGenerator.DATE_PARSER.parse((String) values.get(Field.TIME)));
    }

    protected File getFeedFile() {
        return helper.getFeedFile(getClass().getSimpleName() + "-" + System.nanoTime());
    }

    protected Map<Field, Object> createEntry(int number) throws MalformedURLException {
        Map<Field, Object> values = new HashMap<Field, Object>();
        values.put(Field.NAME, "entryName : " + f.getName() + "-" + number);
        values.put(Field.DESCRIPTION, "entryDescription : " + f.getName() + "-" + number);
        values.put(Field.AUTHOR, "author-" + number);
        values.put(Field.LINK, f.toURI().toURL() + "/entry-" + number);
        values.put(Field.TIME, RSSGenerator.DATE_PARSER.format(new Date()));
        return values;
    }

}
