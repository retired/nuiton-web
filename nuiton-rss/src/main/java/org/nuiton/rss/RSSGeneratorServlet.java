/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Servlet permettant de generer des flux generes par RSSGenerator et de
 * les publier.
 *
 * @author chemit
 */
public class RSSGeneratorServlet extends BaseServlet<RSSGenerator> {

    private static final long serialVersionUID = 1L;

    public void doCreateFeed(HttpServletRequest request, HttpServletResponse response, String feedName, File file) throws ServletException, IOException {
        String type = request.getParameter("feedType");
        FeedType feedType = null;
        if (type != null) {
            try {
                feedType = FeedType.valueOf(type);
            } catch (Exception e) {
                feedType = null;
            }
        }
        if (feedType == null) {
            feedType = delegate.getFormat();
        }
        String description = request.getParameter("feedDescription");
        String link = request.getParameter("feedLink");
        String author = request.getParameter("feedAuthor");
        String categories = request.getParameter("feedCategories");
        Map<Field, Object> values = new HashMap<Field, Object>();
        values.put(Field.NAME, feedName);
        addFieldValue(Field.DESCRIPTION, description, values);
        addFieldValue(Field.LINK, link, values);
        addFieldValue(Field.AUTHOR, author, values);
        addFieldValue(Field.CATEGORY, categories, values);
        values.put(Field.TIME, RSSGenerator.DATE_PARSER.format(new Date()));
        try {
            delegate.createFeedFile(file.toURI().toURL(), feedType, values);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

        String redirect = request.getParameter("from");
        if (redirect != null) {
            response.sendRedirect(redirect);
        }
    }

    public void doDeleteFeed(HttpServletRequest request, HttpServletResponse response, String feedName, File file) throws ServletException, IOException {
        try {
            delegate.deleteFeedFile(file.toURI().toURL());
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

        String redirect = request.getParameter("from");
        if (redirect != null) {
            response.sendRedirect(redirect);
        }
    }

    public void doAddItem(HttpServletRequest request, HttpServletResponse response, String feedName, File file) throws ServletException, IOException {

        Integer nbItems = convertToInt(request.getParameter("nbItems"));
        if (nbItems == null) {
            nbItems = RSSGeneratorConfig.DEFAULT_GENERATOR_NB_ITEM;
        }
        String description = request.getParameter("itemDescription");
        String link = request.getParameter("itemLink");
        String author = request.getParameter("itemAuthor");
        String name = request.getParameter("itemName");

        Map<Field, Object> values = new HashMap<Field, Object>();
        addFieldValue(Field.NAME, name, values);
        addFieldValue(Field.DESCRIPTION, description, values);
        addFieldValue(Field.LINK, link, values);
        addFieldValue(Field.AUTHOR, author, values);
        values.put(Field.TIME, RSSGenerator.DATE_PARSER.format(new Date()));
        try {
            delegate.addItemToFeedFile(file.toURI().toURL(), nbItems, values);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

        String redirect = request.getParameter("from");
        if (redirect != null) {
            response.sendRedirect(redirect);
        }
    }

    public void doList(HttpServletResponse response) throws IOException {
        // obtain the list of known feeds
        File[] files = delegate.getGeneratorDirectory().listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                return pathname.isFile() && pathname.getName().endsWith(".xml");
            }
        });
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            if (files.length > 0) {
                String format = "<option value=\"%1$s\">%1$s</option>";
                for (File f : files) {
                    String name = f.getName();
                    out.println(String.format(format, name.substring(0, name.length() - 4)));
                }
            } else {
                out.println("<span class='error'>no feed generated</span>");
            }
        } finally {
            out.close();
        }
    }

    public void doShow(File file, String feedName, HttpServletResponse response) throws ServletException, IOException {
        // no action, just publication
        if (!file.exists()) {
            throw new ServletException("could not find feed " + feedName);
        }
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            SyndFeed feed = RSSIOUtil.readFeed(file.toURI().toURL());
            RSSIOUtil.saveFeed(out, feed);
        } catch (IllegalArgumentException ex) {
            throw new ServletException(ex);
        } catch (FeedException ex) {
            throw new ServletException(ex);
        } finally {
            out.close();
        }
    }

    @Override
    protected ConfigInitializer<ServletConfig, ?> newConfigInitializer() {
        return new RSSGeneratorConfig.RssGeneratorConfigInitializer<ServletConfig>() {

            protected String getConfigValue(ServletConfig config, String fullConfigName) {
                return config.getInitParameter(fullConfigName);
            }
        };
    }

    @Override
    protected RSSGenerator newDelegate() throws Exception {
        return RSSGenerator.newDefaultInstance();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException      TODO
     * @throws ServletException TODO
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }
        action = action.trim().toLowerCase();
        if ("list".equals(action)) {
            doList(response);
            return;
        }

        String feedName = request.getParameter("feedName");

        if (feedName == null || "".equals(feedName)) {
            throw new ServletException("could not find feedName parameter");
        }
        //feedName =new String(feedName.getBytes(),0,feedName.length(),Charset.forName("utf-8"));
        File file = new File(delegate.getGeneratorDirectory(), feedName + ".xml");

        if ("show".equals(action)) {
            doShow(file, feedName, response);
            return;
        }

        if ("create".equals(action)) {
            doCreateFeed(request, response, feedName, file);
            return;
        }

        if ("delete".equals(action)) {
            doDeleteFeed(request, response, feedName, file);
            return;
        }

        if ("additem".equals(action)) {
            doAddItem(request, response, feedName, file);
            return;
        }

        throw new ServletException("action '" + action + "' is unknown ");

    }

    protected void addFieldValue(Field field, String value, Map<Field, Object> values) {
        if (value != null && !"".equals(value.trim())) {
            values.put(field, value);
        }
    }
}
