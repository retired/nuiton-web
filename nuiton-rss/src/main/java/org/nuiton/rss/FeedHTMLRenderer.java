/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author poussin
 */
public class FeedHTMLRenderer implements FeedRenderer {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    private static final Log log = LogFactory.getLog(FeedHTMLRenderer.class);

    private static final String FEED_CSS_PREFIX = "feed-";
    private static final String FEED_ITEM_CSS_PREFIX = "feedItem-";

    /**
     * tous les HTMLCleaner a utiliser pour nettoyer les chaines
     */
    protected Set<HTMLCleaner> cleaners = new HashSet<HTMLCleaner>();

    protected EnumMap<Field, String> templates = new EnumMap<Field, String>(Field.class);
    protected EnumMap<Field, String> classnames = new EnumMap<Field, String>(Field.class);
    protected EnumMap<Field, String> feedProperties = new EnumMap<Field, String>(Field.class);
    protected EnumMap<Field, String> itemProperties = new EnumMap<Field, String>(Field.class);

    /**
     * @param cleaners La liste des cleaners a utiliser par defaut. Si aucun
     *                 cleaner n'est passer en argument HTMLScriptCleaner est automatiquement
     *                 ajoute
     */
    public FeedHTMLRenderer(HTMLCleaner... cleaners) {
        if (cleaners.length == 0) {
            addHTMLCleaner(new HTMLScriptCleaner());
        }
        for (HTMLCleaner cleaner : cleaners) {
            addHTMLCleaner(cleaner);
        }

        templates.put(Field.NAME, "<span class=\"%1$s\">%2$s</span>\n");
        templates.put(Field.LINK, "<a class=\"%1$s\" href=\"%2$s\">%3$s</a>");
        templates.put(Field.IMAGE, "<span class=\"%1$s\"><img src=\"%2$s\"/></span>\n");
        templates.put(Field.DESCRIPTION, "<span class=\"%1$s\">%2$s</span>\n");
        templates.put(Field.AUTHOR, "<span class=\"%1$s\">%2$s</span>\n");
        //Probleme de conversion FIXME
        templates.put(Field.TIME, "<span class=\"%1$s\">%2$s</span>\n");
        //templates.put(Field.TIME, "<span class=\"%1$s\">%2$tF</span>\n");

        classnames.put(Field.NAME, "name");
        classnames.put(Field.LINK, "link");
        classnames.put(Field.IMAGE, "image");
        classnames.put(Field.DESCRIPTION, "description");
        classnames.put(Field.AUTHOR, "author");
        classnames.put(Field.TIME, "time");

        feedProperties.put(Field.NAME, "title");
        feedProperties.put(Field.LINK, "link");
        // cela engendre des NPE...
        feedProperties.put(Field.IMAGE, "image.url");
        feedProperties.put(Field.DESCRIPTION, "description");
        feedProperties.put(Field.AUTHOR, "author");
        feedProperties.put(Field.TIME, "publishedDate");

        itemProperties.put(Field.NAME, "title");
        itemProperties.put(Field.LINK, "link");
        itemProperties.put(Field.IMAGE, null);
        itemProperties.put(Field.DESCRIPTION, "description.value");
        itemProperties.put(Field.AUTHOR, "author");
        itemProperties.put(Field.TIME, "publishedDate");
    }


    public FeedHTMLRenderer() {
        this(new HTMLCleaner[0]);
    }

    /**
     * Supprime tous les cleaners de code html
     */
    public void clearCleaners() {
        cleaners.clear();
    }

    /**
     * Ajoute un nouveau cleaner de code HTML
     *
     * @param cleaner cleaner to add
     */
    public void addHTMLCleaner(HTMLCleaner cleaner) {
        cleaners.add(cleaner);
    }

    public String render(FeedRendererConfig config, SyndFeed feed, SyndEntry[] items) {

        StringBuffer sbGlobal = new StringBuffer();

        StringBuffer sbTemp = new StringBuffer();
        for (Field f : config.getFeedFieldOrder()) {
            renderField(f, FEED_CSS_PREFIX, feedProperties, feed, sbTemp);
        }

        String tmp = sbTemp.toString();
        if (!"".equals(tmp)) {
            sbGlobal.append("<div class='rss-feed'>\n").append(tmp).append("</div>\n");
        }
        sbGlobal.append("<ul class='rss-items'>\n");
        sbTemp = new StringBuffer();
        for (SyndEntry item : items) {
            sbTemp.append("<li>\n");
            for (Field f : config.getItemFieldOrder()) {
                renderField(f, FEED_ITEM_CSS_PREFIX, itemProperties, item, sbTemp);
            }
            sbTemp.append("</li>\n");
        }
        sbGlobal.append(sbTemp.toString()).append("</ul>\n");

        return sbGlobal.toString();
    }

    protected void renderField(Field f, String cssPrefix, EnumMap<Field, String> properties, Object src, StringBuffer buffer) {

        if (f == Field.LINK) {
            // do nothing, link should not be called alone but inside name field only
            return;
        }
        String template = templates.get(f);
        String classname = cssPrefix + classnames.get(f);
        String prop = properties.get(f);

        if ("".equals(prop)) {
            return;
        }

        String result;
        Object value = null;
        try {

            value = BeanUtils.getProperty(src, prop);
            if (f == Field.NAME) {
                //special case, we must first render link
                String templateLink = templates.get(Field.LINK);
                String classnameLink = "feed-" + classnames.get(Field.LINK);
                String propLink = properties.get(Field.LINK);
                Object value2 = BeanUtils.getProperty(src, propLink);
                value = render(templateLink, classnameLink, value2, value);
            }

            result = render(template, classname, value);

            if (result != null && buffer != null) {
                buffer.append(result);
            }
        } catch (Exception eee) {
            log.warn("Can't get feed property value for property " + prop + ", value:" + value);
        }

    }

    /**
     * Permet de rendre un element du feed
     *
     * @param template  le template pour rendre l'element
     * @param classname la class de l'element html (pour une utilisation CSS)
     * @param value     la valeur de l'element
     * @return le code HTML
     */
    protected String render(String template, String classname, Object value) {
        StringBuffer sb = new StringBuffer();
        if (value != null && !"".equals(value)) {
            if (value instanceof String) {
                // si value est une chaine, on la nettoie avec les cleaners enregistrer
                for (HTMLCleaner cleaner : cleaners) {
                    value = cleaner.clean((String) value);
                }
            }
            sb.append(renderTemplate(template, classname, value));
        }
        return sb.toString();
    }

    protected String render(String template, String classname, Object value, Object value2) {
        StringBuffer sb = new StringBuffer();
        if (value != null && !"".equals(value)) {
            if (value instanceof String) {
                // si value est une chaine, on la nettoie avec les cleaners enregistrer
                for (HTMLCleaner cleaner : cleaners) {
                    value = cleaner.clean((String) value);
                }
            }
            sb.append(renderTemplate(template, classname, value, value2));
        }
        return sb.toString();
    }

    protected String renderTemplate(String template, Object... args) {
        try {
            String result;
            result = String.format(template, args);
            return result;
        } catch (Exception e) {
            log.warn("could not format template " + template + " with args : " + Arrays.toString(args));
            return "";
        }
    }

}
