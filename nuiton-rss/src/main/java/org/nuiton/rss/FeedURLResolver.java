/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import java.net.MalformedURLException;

/**
 * Permet de convertir une chaine en une URL. Le resolver le plus simple
 * peut créer une URL avec la chaine, mais un autre resolver peut utiliser
 * cette chaine comme cle dans une base de données pour recuperer la bonne URL.
 *
 * @author poussin
 */
public interface FeedURLResolver {

    FeedData resolv(String ask) throws MalformedURLException;
}
