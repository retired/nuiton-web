/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

/**
 * Default configuration for {@link RSSHelper}.
 *
 * @author poussin
 */
public class RSSConfig {

    /**
     * la propriete de configuration qui definit le prefix a jouter pour scruter la configuration
     */
    private static final String CONFIGURATION_PREFIX_PROPERTY = RSSServlet.class.getSimpleName() + "_configuration_prefix";


    /**
     * Temps a partir duquel on reforce une recuperation
     */
    public static long TIME_FORCE_RETRIEVED = 5 * 60 * 1000;
    public static FeedRendererConfig DEFAULT_RENDERER_CONFIG = new FeedRendererConfig("n-natd");
    public static int DEFAULT_NB_ITEM = 7;
    public static Class<? extends FeedRenderer> DEFAULT_RENDERER_CLASS = FeedHTMLRenderer.class;
    public static Class<? extends FeedURLResolver> DEFAULT_RESOLVER_CLASS = SimpleFeedURLResolver.class;

    public void setTIME_FORCE_RETRIEVED(long TIME_FORCE_RETRIEVED) {
        RSSConfig.TIME_FORCE_RETRIEVED = TIME_FORCE_RETRIEVED;
    }

    public void setDEFAULT_RENDERER_CONFIG(String DEFAULT_RENDERER_CONFIG) {
        RSSConfig.DEFAULT_RENDERER_CONFIG = new FeedRendererConfig(DEFAULT_RENDERER_CONFIG);
    }

    public void setDEFAULT_NBITEM(int DEFAULT_NBITEM) {
        RSSConfig.DEFAULT_NB_ITEM = DEFAULT_NBITEM;
    }

    public void setDEFAULT_RENDERER_CLASS(Class<? extends FeedRenderer> DEFAULT_RENDERER_CLASS) {
        RSSConfig.DEFAULT_RENDERER_CLASS = DEFAULT_RENDERER_CLASS;
    }

    public void setDEFAULT_RESOLVER_CLASS(Class<? extends FeedURLResolver> DEFAULT_RESOLVER_CLASS) {
        RSSConfig.DEFAULT_RESOLVER_CLASS = DEFAULT_RESOLVER_CLASS;
    }

    /**
     * @param <S> la classe source de la configuration
     */
    public static abstract class RSSConfigInitializer<S> extends ConfigInitializer<S, RSSConfig> {

        /**
         * Initialise RSSConfig.
         *
         * @param config la configuration source
         */
        public void init(S config) {
            RSSHelper.log.info("with source " + config);
            super.init(CONFIGURATION_PREFIX_PROPERTY, RSSConfig.class, config,
                    "TIME_FORCE_RETRIEVED",
                    "DEFAULT_RENDERER_CONFIG",
                    "DEFAULT_NB_ITEM",
                    "DEFAULT_RENDERER_CLASS",
                    "DEFAULT_RESOLVER_CLASS");
        }
    }
}
