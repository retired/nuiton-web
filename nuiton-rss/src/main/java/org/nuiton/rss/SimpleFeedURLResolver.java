/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Par defaut retourne un FeedData ou la chaine passer est convertie en URL,
 * le nombre d'item est positionne a -1 et le renderer a n-natd
 *
 * @author poussin
 */
public class SimpleFeedURLResolver implements FeedURLResolver {

    public FeedData resolv(String ask) throws MalformedURLException {
        URL url = new URL(ask);
        FeedData result = new FeedData(url);
        result.setNbItem(RSSConfig.DEFAULT_NB_ITEM);
        result.setRendererConfig(RSSConfig.DEFAULT_RENDERER_CONFIG);
        return result;
    }

}
