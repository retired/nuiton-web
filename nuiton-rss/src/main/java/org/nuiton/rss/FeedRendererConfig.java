/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Permet de spcecifier les champs a rendre, cet objet peut etre converti
 * en une representation chaine pour etre facilement transportable.
 * </p>
 *
 * Exemple de representation texte: ndat-ndat. Le premier block represente le
 * feed le second les items. S'il n'y a qu'un block cela veut dire qu'il ne
 * faut pas reprensenter le feed mais seulement les items. L'ordre des lettres
 * determine l'ordre du rendu.
 * <ul>
 * <li>n: name pour demander le nom ou titre du feed ou de l'item</li>
 * <li>i: image</li>
 * <li>d: description</li>
 * <li>a: author</li>
 * <li>t: time pour demander la date du feed ou de l'item</li>
 * </ul>
 *
 * @author poussin
 */
public class FeedRendererConfig {

    protected String feedString = "indat";
    protected List<Field> feedList;
    protected String itemString = "indat";
    protected List<Field> itemList;

    public FeedRendererConfig() {
    }

    public FeedRendererConfig(String representation) {
        setString(representation);
    }

    protected List<Field> createList(String value) {
        List<Field> result = new ArrayList<Field>();
        for (char c : value.toCharArray()) {
            Field field = Field.valueOf(c);
            if (field != null) {
                result.add(field);
            }
        }
        return result;
    }

    public List<Field> getFeedFieldOrder() {
        if (feedList == null) {
            feedList = createList(feedString);
        }
        return feedList;
    }

    public List<Field> getItemFieldOrder() {
        if (itemList == null) {
            itemList = createList(itemString);
        }
        return itemList;
    }

    /**
     * Permet de modifier la representation souhaitee
     *
     * @param f la nouvelle representation par ex: n-na
     */
    public void setString(String f) {
        String[] fi = f.split("-");
        if (fi.length > 1) {
            feedString = fi[0];
            itemString = fi[1];
        } else {
            feedString = "";
            itemString = fi[0];
        }
        feedList = null;
        itemList = null;
    }

    /**
     * Permet de retourner la representation string
     *
     * @return par ex: n-nad
     */
    public String getString() {
        String result = "";
        if (!"".equals(feedString)) {
            result = feedString + "-";
        }
        result += itemString;
        return result;
    }

}
