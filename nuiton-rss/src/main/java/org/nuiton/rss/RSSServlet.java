/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * <p>
 * Servlet permettant de retourner un feed RSSHelper en une representation HTML.
 * Si la servlet est appele sans parametre alors le code JS permettant d'utiliser
 * cette servlet en Ajax est retourne.
 * </p>
 * Sinon la servlet peut prendre en parametre:
 * <ul>
 * <li> feedName: le nom du feed souhaite</li>
 * <li> nbItem: le nombre d'item du feed a retourner</li>
 * <li> feedRepr: le chaine permettant de connaitre les champs du feed voulu</li>
 * </ul>
 *
 * Utilisation dans une page HTML. Dans le header ajouter
 * <pre>
 *   &lt;script src="url/to/RSSServlet"&gt;&lt;/script&gt;
 * </pre>
 * Dans le corps placer un element div avec un id particulier.
 * Et soit a la fin de la page soit juste apres le div, ajouter le script
 * <pre>
 * rssinclude('feedName', 'divid', 5, 'n-n');
 * </pre>
 *
 * @author poussin
 */
public class RSSServlet extends BaseServlet<RSSHelper> {

    /**
     * Le code JavaScript a utilise dans les pages clientes
     * <p>
     * Provient du fichier rssinclude.js
     * cat rssinclude.js |sed 's/"/\\"/g' |sed -re 's/^(.*)$/"\1\\n" +/'
     */
    protected static String js =
            "function rssincludeUpdateDiv(div, background) {\n" +
                    "    // mettre background a false est utile lorsque l'on appelle la methode\n" +
                    "    // depuis une fenetre que l'on ferme ensuite\n" +
                    "    var serlvetUrl = \"%1$s\";    \n" +
                    "    win = window;\n" +
                    "    var xhr = null;\n" +
                    "    var error=null;\n" +
                    "    var feedName = div.getAttribute(\"feedName\");\n" +
                    "    if (!feedName) // pas de feed name, fatal error\n" +
                    "        error = \"<span class='feed-error'>Aucun feed name detecte...</span>\";        \n" +
                    "    else if(win.XMLHttpRequest) // Firefox, Opera detected\n" +
                    "        xhr = new win.XMLHttpRequest();\n" +
                    "    else if(win.ActiveXObject) // Internet Explorer detected\n" +
                    "        xhr = new win.ActiveXObject(\"Microsoft.XMLHTTP\");\n" +
                    "    else  // XMLHttpRequest non supporte par le navigateur\n" +
                    "        error = \"<span class='feed-error'>Votre navigateur ne supporte pas les objets XMLHTTPRequest...</span>\";    \n" +
                    "    if (!!error) { div.innerHTML = error; return; } \n" +
                    "    \n" +
                    "    var nbItem = div.getAttribute(\"nbItem\");\n" +
                    "    var feedRepr = div.getAttribute(\"feedRepr\");            \n" +
                    "    var forceReload = div.getAttribute(\"forceReload\");            \n" +
                    "    var url = serlvetUrl+(serlvetUrl.indexOf('?')>-1?'&':'?')+\"feedName=\" + escape(feedName);        \n" +
                    "    if (!!nbItem) {url += '&nbItem=' + escape(nbItem);}        \n" +
                    "    if (!!feedRepr) {url += '&feedRepr=' + escape(feedRepr);}\n" +
                    "    if (!!forceReload) {url += '&forceReload=true';}\n" +
                    "\n" +
                    "    xhr.open(\"GET\", url, !!background);\n" +
                    "    xhr.onreadystatechange = function() {\n" +
                    "        if(xhr.readyState == 1) div.innerHTML = \"Chargement du flux '\"+feedName+\"'\";\n" +
                    "        if(xhr.readyState == 4) div.innerHTML = xhr.responseText;\n" +
                    "    }\n" +
                    "    xhr.send(null);\n" +
                    "}\n" +
                    "var initRss =function () {\n" +
                    "    var divs = document.getElementsByTagName(\"div\");    \n" +
                    "    var i = 0;var max=divs.length;  \n" +
                    "    var toTreate = [];\n" +
                    "    while (i < max) {\n" +
                    "        var div = divs[i++];\n" +
                    "        if (div.getAttribute('name')=='rssinclude') toTreate[toTreate.length] = div;\n" +
                    "    }\n" +
                    "    i=0;max = toTreate.length;\n" +
                    "    while (i<max) rssincludeUpdateDiv(toTreate[i++], true);" +
                    "}\n" +
                    "if (window.addEventListener) window.addEventListener( 'load', initRss,false);\n" +
                    "else if (window.attachEvent) window.attachEvent( 'onload', initRss);";

    private static final long serialVersionUID = 1L;

    /**
     * @param url current url to add in script
     * @return the rssinclude script, patched with url
     */
    public static String getJs(String url) {
        String code = String.format(js, url);
        return code;
    }

    public void doJs(HttpServletResponse response, HttpServletRequest request) throws IOException {
        // on renvoie le code js
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String code = getJs(request.getRequestURL().toString());
            out.println(code);
        } finally {
            out.close();
        }
    }

    public void doRender(HttpServletResponse response, HttpServletRequest request, String feedName) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String feedRepr = request.getParameter("feedRepr");
            Integer nbItem = convertToInt(request.getParameter("nbItem"));
            boolean forceReload = "true".equalsIgnoreCase(request.getParameter("forceReload"));
            out.println(delegate.getHTML(feedName, feedRepr, nbItem, forceReload));
        } finally {
            out.close();
        }
    }

    @Override
    protected ConfigInitializer<ServletConfig, ?> newConfigInitializer() {
        return new RSSConfig.RSSConfigInitializer<ServletConfig>() {

            protected String getConfigValue(ServletConfig config, String fullConfigName) {
                return config.getInitParameter(fullConfigName);
            }
        };
    }

    @Override
    protected RSSHelper newDelegate() throws Exception {
        return RSSHelper.newDefaultInstance();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException      TODO
     * @throws ServletException TODO
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String feedName = request.getParameter("feedName");
        if (feedName == null || "".equals(feedName)) {
            doJs(response, request);
        } else {
            doRender(response, request, feedName);
        }
    }
}
