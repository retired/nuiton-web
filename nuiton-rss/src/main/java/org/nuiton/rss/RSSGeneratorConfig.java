/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import java.io.File;

/**
 * Default Configuration  for {@link RSSGenerator}.
 *
 * @author chemit
 */
public class RSSGeneratorConfig {

    /**
     * la propriete de configuration qui definit le prefix a jouter pour scruter la configuration
     */
    private static final String CONFIGURATION_PREFIX_PROPERTY = RSSGeneratorServlet.class.getSimpleName() + "_configuration_prefix";
    /**
     * nombre d'items max à ecrire dans le fichier du flux
     */
    public static int DEFAULT_GENERATOR_NB_ITEM = 100;
    /**
     * le format par défaut duflux à générer
     */
    public static String DEFAULT_GENERATOR_FORMAT = "RSS_2_0";
    /**
     * le répertoire où générer les flux
     */
    public static String GENERATOR_DIRECTORY = "/tmp/rssinclude";
    /**
     * le caractère séparateur des listes de métas (categorie par exemple)
     */
    public static String DEFAULT_LIST_SEPARATOR = ";";

    public void setDEFAULT_GENERATOR_NB_ITEM(int DEFAULT_GENERATOR_NB_ITEM) {
        RSSGeneratorConfig.DEFAULT_GENERATOR_NB_ITEM = DEFAULT_GENERATOR_NB_ITEM;
    }

    public void setDEFAULT_GENERATOR_FORMAT(String DEFAULT_GENERATOR_FORMAT) {
        RSSGeneratorConfig.DEFAULT_GENERATOR_FORMAT = DEFAULT_GENERATOR_FORMAT;
    }

    public void setGENERATOR_DIRECTORY(String GENERATOR_DIRECTORY) {
        RSSGeneratorConfig.GENERATOR_DIRECTORY = GENERATOR_DIRECTORY;
    }

    public void setDEFAULT_LIST_SEPARATOR(String DEFAULT_LIST_SEPARATOR) {
        RSSGeneratorConfig.DEFAULT_LIST_SEPARATOR = DEFAULT_LIST_SEPARATOR;
    }

    /**
     * @param <S> la classe source de la configuration
     */
    public static abstract class RssGeneratorConfigInitializer<S> extends ConfigInitializer<S, RSSGeneratorConfig> {

        /**
         * Initialise RSSGeneratorConfig.
         *
         * @param config la configuration source
         */
        public void init(S config) {
            RSSGenerator.log.info("with source " + config);

            super.init(CONFIGURATION_PREFIX_PROPERTY, RSSGeneratorConfig.class, config,
                    "DEFAULT_GENERATOR_NB_ITEM",
                    "DEFAULT_GENERATOR_FORMAT",
                    "DEFAULT_LIST_SEPARATOR",
                    "GENERATOR_DIRECTORY");

            // create delegate directory
            new File(GENERATOR_DIRECTORY).mkdirs();

        }
    }
}
