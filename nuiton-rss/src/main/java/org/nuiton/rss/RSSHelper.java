/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.MalformedURLException;
import java.util.List;

/**
 * @author poussin
 */
public class RSSHelper {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    static final Log log = LogFactory.getLog(RSSHelper.class);

    /**
     * @return une nouvelle instance de RssHelper en utilisant les implantations
     *         definies dans RSSConfig.
     * @throws Exception pour tout pb lors de l'instanciation des objects
     */
    public static RSSHelper newDefaultInstance() throws Exception {
        FeedURLResolver r = RSSConfig.DEFAULT_RESOLVER_CLASS.newInstance();
        FeedRenderer rr = RSSConfig.DEFAULT_RENDERER_CLASS.newInstance();
        RSSHelper helper = new RSSHelper(r, rr);
        return helper;
    }

    /**
     * La classe permettant de resoudre les urls
     */
    protected FeedURLResolver resolver;
    /**
     * on utilise des soft reference pour eviter de trop utiliser la memoire
     * key: String, value: FeedData
     */
    protected ReferenceMap feeds = new ReferenceMap(ReferenceStrength.HARD, ReferenceStrength.SOFT);
    protected FeedRenderer renderer;

    /**
     * Utilise un SimpleFeedURLResolver comme resolver de feed
     */
    public RSSHelper() {
        this(new SimpleFeedURLResolver(), new FeedHTMLRenderer(new HTMLScriptCleaner()));
    }

    public RSSHelper(FeedURLResolver resolver, FeedRenderer renderer) {
        this.resolver = resolver;
        this.renderer = renderer;
    }

    public void setResolver(FeedURLResolver resolver) {
        this.resolver = resolver;
    }

    public void setRenderer(FeedRenderer renderer) {
        this.renderer = renderer;
    }

    public FeedRenderer getRenderer() {
        return renderer;
    }

    public FeedURLResolver getResolver() {
        return resolver;
    }

    public void removeFeed(String feedName) {
        feeds.remove(feedName);
    }

    /**
     * Permet de recuperer un certain nombre d'item du feed demande
     *
     * @param feedName       nom du feed souhaite, l'url sera resolu avec le FeedURLResolver
     * @param representation indique les champs en sortie souhaites (ex: n-nt)
     * @param nbItem         le nombre d'item du feed souhaite
     * @param forceReload    un flag pour forcer la relecture du feed
     * @return le code html representant le feed
     */
    public String getHTML(String feedName, String representation, Integer nbItem, boolean forceReload) {
        try {
            FeedData feed = (FeedData) feeds.get(feedName);
            if (forceReload || feed == null || feed.getLastRetrived() + RSSConfig.TIME_FORCE_RETRIEVED < System.currentTimeMillis()) {
                feed = resolver.resolv(feedName);
                if (feed == null) {
                    log.warn("could not find feed for " + feedName);
                    return "<span class='feed-error'>No feed found for " + feedName + "</span>";
                }
                feeds.put(feedName, feed);
            }

            FeedRendererConfig c;
            if (representation != null) {
                c = new FeedRendererConfig(representation);
            } else {
                c = feed.getRendererConfig();
            }

            int nb;
            if (nbItem != null) {
                nb = nbItem;
            } else {
                nb = feed.getNbItem();
            }

            SyndFeed sf = feed.getFeed();
            //TODO Deal with NPE... if feed is null
            List<?> syndEntries = sf.getEntries();
            if (nb >= 0 && syndEntries.size() >= nb) {
                syndEntries = syndEntries.subList(syndEntries.size() - nb, syndEntries.size());
            }
            SyndEntry[] items = syndEntries.toArray(new SyndEntry[syndEntries.size()]);

            String result = renderer.render(c, sf, items);
            return result;
        } catch (MalformedURLException eee) {
            log.warn("Can't resolv feed url: " + feedName, eee);
            return "<span class='feed-error'>" + eee.getMessage() + "</span>";
        }
    }
}
