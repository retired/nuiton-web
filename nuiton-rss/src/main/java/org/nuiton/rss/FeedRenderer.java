/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;

/**
 * @author poussin
 */
public interface FeedRenderer {

    /**
     * Retourne une chaine qui est la representation du feed et de ces items
     *
     * @param config la configuration a utiliser pour rendre le feed
     * @param feed   le feed a representer
     * @param items  la liste des items du feed a prendre en compte
     * @return la representation souhaitee
     */
    String render(FeedRendererConfig config, SyndFeed feed, SyndEntry[] items);
}
