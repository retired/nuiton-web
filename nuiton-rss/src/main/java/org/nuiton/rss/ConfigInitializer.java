/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import org.apache.commons.beanutils.BeanUtils;

/**
 * @param <S> la classe source de la configuration
 * @param <D> la classe destination de la configuration
 * @author tony
 */
public abstract class ConfigInitializer<S, D> {


    /**
     * @param config         l'object source contenant les configs
     * @param fullConfigName le nom complet de la propriete recherchee
     * @return la valeur de la propriete ou null si non trouve
     */
    protected abstract String getConfigValue(S config, String fullConfigName);

    public abstract void init(S config);

    /**
     * Initialise RSSConfig.
     *
     * @param prefix
     * @param klass  la class de la configuration de destination
     * @param config la configuration source
     * @param keys
     */
    public void init(String prefix, Class<D> klass, S config, String... keys) {
        try {
            String prefixConfig = getConfigValue(config, prefix);
            prefixConfig = prefixConfig == null ? "" : prefixConfig.trim();

            D instance = klass.newInstance();
            for (String key : keys) {
                loadConfig(config, instance, prefixConfig, key);
            }
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void loadConfig(S config, D instance, String prefix, String configName) {
        String value = getConfigValue(config, prefix + configName);
        if (value != null && !"".equals(value)) {
            try {
                BeanUtils.setProperty(instance, configName, value);
            } catch (Exception ex) {
                throw new IllegalStateException("could not load property " + configName + " from config " + config + " for reason : " + ex.getMessage());
            }
        }
    }
}
