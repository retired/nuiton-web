/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.SyndFeedOutput;
import com.sun.syndication.io.XmlReader;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * helper to read or save a feed
 *
 * @author tony
 */
public class RSSIOUtil {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    static final Log log = LogFactory.getLog(RSSIOUtil.class);

    /**
     * Load a feed from his url
     *
     * @param url location of feed
     * @return the java pojo feed
     * @throws IllegalArgumentException
     * @throws FeedException
     * @throws IOException
     */
    public static SyndFeed readFeed(URL url) throws IllegalArgumentException, FeedException, IOException {
        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feed = input.build(new XmlReader(url));
        return feed;
    }

    /**
     * save a feed into a file.
     *
     * @param file
     * @param feed
     * @throws IOException
     * @throws FeedException
     */
    public static void saveFeed(File file, SyndFeed feed) throws IOException, FeedException {
        SyndFeedOutput output = new SyndFeedOutput();
        output.output(feed, file);
    }

    /**
     * save a feed into a writer.
     *
     * @param writer
     * @param feed
     * @throws IOException
     * @throws FeedException
     */
    public static void saveFeed(Writer writer, SyndFeed feed) throws IOException, FeedException {
        SyndFeedOutput output = new SyndFeedOutput();
        output.output(feed, writer);
    }

    public static SyndFeed createFeed(EnumMap<Field, String> feedProperties, FeedType type, Map<Field, Object> values) throws ParseException {
        SyndFeed feed = new SyndFeedImpl();
        feed.setFeedType(type.getType());
        feed.setEncoding("utf-8");
        fillFeed(values, feedProperties, feed, false);
        return feed;
    }

    public static EnumSet<Field> updateFeed(URL url, EnumMap<Field, String> feedProperties, Map<Field, Object> values) throws ParseException, IllegalArgumentException, FeedException, IOException {
        SyndFeed feed = RSSIOUtil.readFeed(url);
        return updateFeed(feed, feedProperties, values);
    }

    public static EnumSet<Field> updateFeed(SyndFeed feed, EnumMap<Field, String> feedProperties, Map<Field, Object> values) throws ParseException, IllegalArgumentException, FeedException, IOException {
        EnumSet<Field> modifieds = fillFeed(values, feedProperties, feed, true);
        return modifieds;
    }

    @SuppressWarnings({"unchecked"})
    protected static SyndFeed addItemToFeed(SyndFeed feed, SyndEntry item, int nbEntries, Map<Field, Object> values) throws IOException, IllegalArgumentException, FeedException, ParseException {
        List<SyndEntry> entries = feed.getEntries();
        if (!entries.isEmpty()) {
            // always sort by publication date
            Collections.sort(entries, new FeedEntryComparator());
            // keep only nbEntries -1 entries
            while (entries.size() > nbEntries - 1) {
                entries.remove(0);
            }
        }
        entries.add(item);
        if (log.isDebugEnabled()) {
            log.debug("new item " + item);
        }
        return feed;
    }

    public static SyndEntry createFeedItem(EnumMap<Field, String> itemProperties, Map<Field, Object> values) throws ParseException {

        SyndEntry feedEntry = new SyndEntryImpl();

        for (Entry<Field, Object> entry : values.entrySet()) {
            Field field = entry.getKey();
            String name = itemProperties.get(field);
            if (name == null) {
                // this field is not managed
                log.warn("the field " + field + " is not managed in item");
                continue;
            }
            Object value = entry.getValue();
            Object realValue;
            switch (field) {
                case TIME:
                    realValue = RSSGenerator.DATE_PARSER.parse((String) value);
                    break;
                case DESCRIPTION:
                    //TODO Deal with xml content ?
                    SyndContent description = new SyndContentImpl();
                    description.setType("text/plain");
                    feedEntry.setDescription(description);
                    realValue = String.valueOf(value);
                    break;
                default:
                    realValue = value;
            }
            setFieldValue(feedEntry, name, realValue);
        }

        return feedEntry;
    }

    protected static EnumSet<Field> fillFeed(Map<Field, Object> values, EnumMap<Field, String> feedProperties, SyndFeed feed, boolean treateModfied) throws ParseException {
        EnumSet<Field> modifieds = EnumSet.noneOf(Field.class);

        for (Entry<Field, Object> entry : values.entrySet()) {
            Field field = entry.getKey();
            String name = feedProperties.get(field);
            if (name == null) {
                // this field is not managed
                log.warn("the field " + field + " is not managed in feed");
                continue;
            }
            Object value = entry.getValue();
            Object realValue;
            switch (field) {
                case TIME:
                    realValue = RSSGenerator.DATE_PARSER.parse((String) value);
                    break;
                case CATEGORY:
                    List<String> categoriesAsList = new ArrayList<String>();
                    if (value instanceof List) {
                        categoriesAsList = (List) value;
                    } else {
                        // categories as String
                        String cats = String.valueOf(value);
                        String[] categories = cats.split(RSSGeneratorConfig.DEFAULT_LIST_SEPARATOR);
                        categoriesAsList = Arrays.asList(categories);
                    }
                    realValue = new ArrayList<SyndCategory>();
                    for (String cat : categoriesAsList) {
                        SyndCategory scat = new SyndCategoryImpl();
                        scat.setName(cat);
                        ((List) realValue).add(scat);
                    }
                    break;
                default:
                    realValue = value;
            }
            if (treateModfied) {
                Object oldValue = getFieldValue(feed, name);
                if (oldValue == null) {
                    if (realValue != null) {
                        modifieds.add(field);
                    }
                } else {
                    if (!oldValue.equals(realValue)) {
                        modifieds.add(field);
                    }
                }
            }
            setFieldValue(feed, name, realValue);
        }
        return modifieds;
    }

    protected static void setFieldValue(Object dst, String name, Object value) {
        if (value == null) {
            // null value is not managed
            log.warn("null value for field " + name + " is not managed");
            return;
        }
        try {
            BeanUtils.setProperty(dst, name, value);
        } catch (Exception ex) {
            log.warn("could not access property " + name, ex);
        }
    }

    protected static Object getFieldValue(Object dst, String name) {
        try {
            return BeanUtils.getProperty(dst, name);
        } catch (Exception ex) {
            log.warn("could not access property " + name, ex);
            return null;
        }
    }

    protected RSSIOUtil() {
        // no instance
    }
}
