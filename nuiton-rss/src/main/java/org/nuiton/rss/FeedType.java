/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

/**
 * Enumeration of known feed's type.
 * <p>
 * Used to generate some feed files.
 *
 * @author tony
 */
public enum FeedType {

    RSS_0_90("rss_0.90"),
    RSS_0_91("rss_0.91"),
    RSS_0_92("rss_0.92"),
    RSS_0_93("rss_0.93"),
    RSS_1_0("rss_1.0"),
    RSS_2_0("rss_2.0"),
    ATOM_0_3("atom_0.3"),
    ATOM_1_0("atom_1.0");
    private String type;

    public String getType() {
        return type;
    }

    private FeedType(String type) {
        this.type = type;
    }
}
