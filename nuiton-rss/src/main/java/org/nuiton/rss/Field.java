/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

/**
 * Enumeration des champs disponibles pour le renderer
 *
 * @author poussin
 */
public enum Field {

    NAME('n'), IMAGE('i'), DESCRIPTION('d'), AUTHOR('a'), TIME('t'), LINK('l'), CATEGORY('c');

    /**
     * le caractere utilise pour la version compactee d'une configuration de renderer
     */
    private char c;

    Field(char c) {
        this.c = c;
    }

    public char getC() {
        return c;
    }

    public static Field valueOf(char c) {
        for (Field field : values()) {
            if (field.c == c) {
                return field;
            }
        }
        return null;
    }

    public static Field[] getFeedFields() {
        return new Field[]{NAME, IMAGE, DESCRIPTION, AUTHOR, TIME, LINK};
    }

    public static Field[] getItemFields() {
        return new Field[]{NAME, DESCRIPTION, AUTHOR, TIME, LINK};
    }
}

