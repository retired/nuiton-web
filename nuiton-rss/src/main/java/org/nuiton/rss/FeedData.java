/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import com.sun.syndication.feed.synd.SyndFeed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.URL;

/**
 * <p>
 * Represente une URL demandee. Cette objet est conserve durant le temps de vie
 * de la JVM pour permettre de mutualiser la recuperation et la transformation en
 * HTML.
 * </p>
 * <p>
 * Lorsque l'on demande le HTML et que l'url n'a pas encore ete recuperee ou
 * est trop ancienne alors on lance la recuperation. Si deux threads demandent
 * la recuperation, le deuxieme est mis en attente et profitera de la recuperation
 * de l'autre.
 * </p>
 *
 * @author poussin
 */
public class FeedData {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    private static final Log log = LogFactory.getLog(FeedData.class);

    /**
     * url du feed
     */
    protected URL url;
    /**
     * les champs que l'on souhaite visualiser pour le feed
     */
    protected FeedRendererConfig rendererConfig;
    /**
     * Le nombre d'item que l'on souhaite visualiser pour le feed
     */
    protected int nbItem = -1;

    protected SyndFeed feed;
    protected long lastRetrived;

    public FeedData(URL url, int nbItem, String representation) {
        this(url);
        this.nbItem = nbItem;
        rendererConfig = new FeedRendererConfig(representation);
    }

    public FeedData(URL url) {
        this.url = url;
    }

    public long getLastRetrived() {
        return lastRetrived;
    }

    public URL getUrl() {
        return url;
    }

    public FeedRendererConfig getRendererConfig() {
        return rendererConfig;
    }

    public void setRendererConfig(FeedRendererConfig rendererConfig) {
        this.rendererConfig = rendererConfig;
    }

    public int getNbItem() {
        return nbItem;
    }

    public void setNbItem(int nbItem) {
        this.nbItem = nbItem;
    }

    /**
     * Retourne la representation HTML du RSS
     *
     * @return la representation HTML du RSS
     */
    public SyndFeed getFeed() {
        if (feed == null || lastRetrived + RSSConfig.TIME_FORCE_RETRIEVED < System.currentTimeMillis()) {
            forceRetrived();
        }

        return feed;
    }

    synchronized protected void forceRetrived() {
        if (lastRetrived + RSSConfig.TIME_FORCE_RETRIEVED < System.currentTimeMillis()) {
            try {
                feed = RSSIOUtil.readFeed(url);
                lastRetrived = System.currentTimeMillis();
            } catch (Exception eee) {
                log.warn("Can't get feed: " + url, eee);
            }
        }
    }
}
