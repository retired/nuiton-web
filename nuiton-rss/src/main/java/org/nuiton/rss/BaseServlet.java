/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 * Servlet de base
 *
 * @param <D> la classe de l'object de delegation
 * @author chemit
 */
public abstract class BaseServlet<D> extends HttpServlet {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    protected static final Log log = LogFactory.getLog(BaseServlet.class);
    /**
     * L'objet de delegation du metier de la servlet
     */
    protected transient D delegate;
    private static final long serialVersionUID = 1L;

    /**
     * @return the new delegate object to be used by servlet
     * @throws Exception if any problem while instanciation
     */
    protected abstract D newDelegate() throws Exception;

    /**
     * @return a new ConfigInitializer to be used in
     *         {@link #init(ServletConfig, boolean)} method to prepare
     *         default config.
     */
    protected abstract ConfigInitializer<ServletConfig, ?> newConfigInitializer();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException      TODO
     * @throws ServletException TODO
     */
    protected abstract void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    @Override
    public void init(ServletConfig config) throws ServletException {
        init(config, true);
    }

    protected void init(ServletConfig config, boolean initConfig) throws ServletException {
        try {
            super.init(config);
            if (initConfig) {
                // init de la configuration par defaut depuis la config de servlet
                newConfigInitializer().init(config);
            }
            // instanciate delegate
            delegate = newDelegate();
        } catch (Exception eee) {
            log.warn("Can't configure Servlet", eee);
            if (eee instanceof ServletException) {
                throw (ServletException) eee;
            }
            throw new ServletException("Can't configure Servlet", eee);
        }
    }

    protected Integer convertToInt(String parameter) {
        Integer result = null;
        try {
            result = Integer.parseInt(parameter);
        } catch (Exception eee) {
            log.debug("Can't convert to int: '" + parameter + "'", eee);
        }
        return result;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     */
    @Override
    public String getServletInfo() {
        return "RSS Servlet";
    }
    // </editor-fold>
}
