/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2010 CodeLutin, Tony Chemit, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.rss;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

/**
 * generator of rss feeds to be stored in a file
 *
 * @author tony
 */
public class RSSGenerator {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    static final Log log = LogFactory.getLog(RSSGenerator.class);
    /**
     * date formater use to save date in feed and entries
     */
    public static final DateFormat DATE_PARSER = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    /**
     * @param generatorDirectory generator directory
     * @return une nouvelle instance de RssHelper en utilisant les implantations
     *         definies dans RSSGeneratorConfig.
     * @throws Exception pour tout pb lors de l'instanciation des objects
     */
    public static RSSGenerator newDefaultInstance(File generatorDirectory) throws Exception {
        RSSGenerator helper = new RSSGenerator();
        return helper;
    }

    /**
     * @return une nouvelle instance de RssHelper en utilisant les implantations
     *         definies dans RSSGeneratorConfig.
     * @throws Exception pour tout pb lors de l'instanciation des objects
     */
    public static RSSGenerator newDefaultInstance() throws Exception {
        RSSGenerator helper = newDefaultInstance(null);
        return helper;
    }


    /**
     * dictonnary of field &lt;-&gt; property for feed
     */
    protected final EnumMap<Field, String> feedProperties;
    /**
     * dictonnary of field &lt;-&gt; property for entry
     */
    protected final EnumMap<Field, String> itemProperties;
    /**
     * directory where feeds are stored
     */
    protected final File generatorDirectory;
    /**
     * feed format to use *
     */
    protected final FeedType format;
    /**
     * number of feed to use by default
     */
    protected final int nbItems;

    protected RSSGenerator() {
        this(null);
    }

    protected RSSGenerator(EnumMap<Field, String> feedProperties, EnumMap<Field, String> itemProperties, File generatorDirectory, FeedType format, int nbItems) {
        this.feedProperties = feedProperties;
        this.itemProperties = itemProperties;
        this.generatorDirectory = generatorDirectory;
        this.format = format;
        this.nbItems = nbItems;
    }

    protected RSSGenerator(File generatorDirectory) {

        feedProperties = new EnumMap<Field, String>(Field.class);
        feedProperties.put(Field.NAME, "title");
        feedProperties.put(Field.LINK, "link");
        // cela engendre des NPE...
        feedProperties.put(Field.IMAGE, "image.url");
        feedProperties.put(Field.DESCRIPTION, "description");
        feedProperties.put(Field.AUTHOR, "author");
        feedProperties.put(Field.TIME, "publishedDate");
        feedProperties.put(Field.CATEGORY, "categories");

        itemProperties = new EnumMap<Field, String>(Field.class);
        itemProperties.put(Field.NAME, "title");
        itemProperties.put(Field.LINK, "link");
        itemProperties.put(Field.IMAGE, null);
        itemProperties.put(Field.DESCRIPTION, "description.value");
        itemProperties.put(Field.AUTHOR, "author");
        itemProperties.put(Field.TIME, "publishedDate");

        this.generatorDirectory = generatorDirectory == null ? new File(RSSGeneratorConfig.GENERATOR_DIRECTORY) : generatorDirectory;
        format = FeedType.valueOf(RSSGeneratorConfig.DEFAULT_GENERATOR_FORMAT);
        nbItems = RSSGeneratorConfig.DEFAULT_GENERATOR_NB_ITEM;
    }

    /**
     * @param url    location where to create the file
     * @param type   fromat of feed to create
     * @param values properties of the feed
     * @throws ParseException if pb while parsing date
     * @throws IOException    if io pb
     * @throws FeedException  if pb while creating feed
     */
    public void createFeedFile(URL url, FeedType type, Map<Field, Object> values) throws IOException, FeedException, ParseException {
        if (url == null) {
            throw new NullPointerException("can not create a feed with null url");
        }
        if (type == null) {
            throw new NullPointerException("can not create a feed with null feedtype");
        }
        if (values == null || values.isEmpty()) {
            //TODO Should check mandatory values (title, link,...)
            throw new NullPointerException("can not create a feed with null nor empty values dictonnary");
        }
        File f = getFile(url);

        if (f.exists()) {
            throw new IllegalStateException("feed already existing in " + f.getName());
        }

        // make sure parent exists
        f.getParentFile().mkdirs();

        // block until can acquire lock
        FileLock lock = acquireLock(f);

        try {

            SyndFeed feed = RSSIOUtil.createFeed(feedProperties, type, values);

            RSSIOUtil.saveFeed(f, feed);

        } catch (FeedException e) {
            // file must not be created
            f.delete();
            throw e;
        } catch (ParseException e) {
            // file must not be created
            f.delete();
            throw e;
        } finally {
            releaseLock(f, lock);
        }
    }

    /**
     * Add a item to an existing feed file.
     *
     * @param url       location of feed to used
     * @param nbEntries number of maximum entries to be written in feed file
     * @param values    dictionnary of properties to write
     * @throws FeedException  if feed pb
     * @throws IOException    if io pb
     * @throws ParseException if dateparser pb
     */
    public void addItemToFeedFile(URL url, int nbEntries, Map<Field, Object> values) throws IOException, FeedException, ParseException {
        if (url == null) {
            throw new NullPointerException("can not add a feed's entry with null url");
        }
        if (values == null || values.isEmpty()) {
            //TODO Should check mandatory values (title, link,...)
            throw new NullPointerException("can not add a feed's entry  with null nor empty values dictonnary");
        }
        File f = getFile(url);

        if (!f.exists()) {
            throw new FileNotFoundException("file not existing " + f);
        }

        // block until can acquire lock
        FileLock lock = acquireLock(f);

        try {
            // get feed
            SyndFeed feed = RSSIOUtil.readFeed(url);

            // create item
            SyndEntry item = RSSIOUtil.createFeedItem(itemProperties, values);

            // add item 
            feed = RSSIOUtil.addItemToFeed(feed, item, nbEntries, values);

            // save feed into a tmp file
            File tmpFile = new File(f.getAbsolutePath() + "-tmp_" + System.nanoTime());

            RSSIOUtil.saveFeed(tmpFile, feed);

            // move tmpFile to real file
            tmpFile.renameTo(f);
        } finally {
            releaseLock(f, lock);
        }
    }

    public void deleteFeedFile(URL toURL) {
        File f = getFile(toURL);
        if (f.exists() && !f.delete()) {
            throw new IllegalStateException("could not delete feed " + f.getName());
        }
    }

    /**
     * @param url
     * @param values
     * @return the set of modified fields.
     * @throws IOException
     * @throws FeedException
     * @throws ParseException
     */
    public EnumSet<Field> updateFeedFile(URL url, Map<Field, Object> values) throws IOException, FeedException, ParseException {
        if (url == null) {
            throw new NullPointerException("can not add a feed's entry with null url");
        }
        if (values == null || values.isEmpty()) {
            //TODO Should check mandatory values (title, link,...)
            throw new NullPointerException("can not add a feed's entry  with null nor empty values dictonnary");
        }
        File f = getFile(url);

        if (!f.exists()) {
            throw new FileNotFoundException("file not existing " + f);
        }

        // block until can acquire lock
        FileLock lock = acquireLock(f);

        try {

            SyndFeed feed = RSSIOUtil.readFeed(url);

            // update feed and keep trace of modified fields
            EnumSet<Field> modifieds = RSSIOUtil.updateFeed(feed, feedProperties, values);

            // save feed into a tmp file
            File tmpFile = new File(f.getAbsolutePath() + "-tmp_" + System.nanoTime());

            RSSIOUtil.saveFeed(tmpFile, feed);

            // move tmpFile to real file
            tmpFile.renameTo(f);

            // feed link has changed 
            return modifieds;
        } finally {
            releaseLock(f, lock);
        }
    }

    public FeedType getFormat() {
        return format;
    }

    public File getFeedFile(String name) {
        return new File(getGeneratorDirectory(), name + ".xml");
    }

    public int getNbItems() {
        return nbItems;
    }

    /**
     * Obtain the file from his url location.
     *
     * @param url location of the file
     * @return the file
     * @throws IllegalStateException if uri is not sytax valid
     */
    protected File getFile(URL url) throws IllegalStateException {
        try {
            return new File(url.toURI());
        } catch (URISyntaxException e) {
            throw new IllegalStateException("could not obtain file from url " + url, e);
        }
    }

    protected FileLock acquireLock(File f) throws IOException {
        File lockFile = getWriteFileLock(f);

        /*if (!lockFile.exists()) lockFile.createNewFile(); */
        // open file for writing only
        FileChannel channel = new RandomAccessFile(lockFile, "rw").getChannel();

        // block until can acquire lock

        return channel.lock();
    }

    protected File getWriteFileLock(File f) {
        return new File(f.getParentFile(), f.getName() + ".wlock");
    }

    protected void releaseLock(File f, FileLock lock) throws IOException {
        // release lock
        lock.release();
        // close channel
        lock.channel().close();
        // delete file lock
        File lockFile = getWriteFileLock(f);
        // delete lock file
        lockFile.delete();
    }

    public File getGeneratorDirectory() {
        return generatorDirectory;
    }

}
