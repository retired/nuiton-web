/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
function rssgeneratorUpdateDiv(divs) {
    var serlvetUrl = "RSSGeneratorServlet?action=list";
    win = window;
    var xhr = null;
    var error=null;
    if(win.XMLHttpRequest) // Firefox, Opera detected
        xhr = new win.XMLHttpRequest();
    else if(win.ActiveXObject) // Internet Explorer detected
        xhr = new win.ActiveXObject("Microsoft.XMLHTTP");
    else  // XMLHttpRequest non supporte par le navigateur
        error = "<span class='feed-error'>Votre navigateur ne supporte pas les objets XMLHTTPRequest...</span>";    
    if (!!error) { updateDiv(divs,false,error); return; } 
    
    xhr.open("GET", serlvetUrl, true);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 1) { updateDiv(divs,false,"Chargement de la liste des feeds en cours..."); }
        else if(xhr.readyState == 4) { updateDiv(divs,true,xhr.responseText); }
    }
    xhr.send(null);
}
    
var initRssGenerator =function () {
    var divs = document.getElementsByTagName("div");    
    var i = 0;var max=divs.length;  
    var toTreate = [];
    while (i < max) {
        var div = divs[i++];
        if (div.getAttribute('name')=='rssgeneratorlist') toTreate[toTreate.length] = div;
    }    
    rssgeneratorUpdateDiv(toTreate);
}
var updateDiv= function(divs,select,text) {
    var i=0;
    var max = divs.length;
    while (i<max) {
        var divText=text;
        var div = divs[i++];
        if (!!select && text.indexOf("error")==-1) {
            var name = div.getAttribute("action");    
            divText = "<select name='"+name+"' length='50'>" + text+"</select>"
        }
        div.innerHTML =divText;
    }
}
// load initRssGenerator
if (window.addEventListener) window.addEventListener( 'load', initRssGenerator,false);
else if (window.attachEvent) window.attachEvent( 'onload', initRssGenerator);
