/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
function rssincludeUpdateDiv(div, background) {
    // mettre background a false est utile lorsque l'on appelle la methode
    // depuis une fenetre que l'on ferme ensuite
    //var serlvetUrl = "%1$1";    
    var serlvetUrl = "http://localhost:8083/nuitonrss-2.4/RSSServlet";    
    win = window;
    var xhr = null;
    var error=null;
    var feedName = div.getAttribute("feedName");
    if (!feedName) // pas de feed name, fatal error
        error = "<span class='feed-error'>Aucun feed name detecte...</span>";        
    else if(win.XMLHttpRequest) // Firefox, Opera detected
        xhr = new win.XMLHttpRequest();
    else if(win.ActiveXObject) // Internet Explorer detected
        xhr = new win.ActiveXObject("Microsoft.XMLHTTP");
    else  // XMLHttpRequest non supporte par le navigateur
        error = "<span class='feed-error'>Votre navigateur ne supporte pas les objets XMLHTTPRequest...</span>";    
    if (!!error) { div.innerHTML = error; return; } 
    
    var nbItem = div.getAttribute("nbItem");
    var feedRepr = div.getAttribute("feedRepr");            
    var forceReload = div.getAttribute("forceReload");            
    var url = serlvetUrl+(serlvetUrl.indexOf('?')>-1?'&':'?')+"feedName=" + escape(feedName);        
    if (!!nbItem) {url += '&nbItem=' + escape(nbItem);}        
    if (!!feedRepr) {url += '&feedRepr=' + escape(feedRepr);}
    if (!!forceReload) {url += '&forceReload=true';}

    xhr.open("GET", url, !!background);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 1) div.innerHTML = "Chargement du flux '"+feedName+"' en cours...";
        if(xhr.readyState == 4) div.innerHTML = xhr.responseText;
    }
    xhr.send(null);
}
    
var initRss =function () {
    var divs = document.getElementsByTagName("div");    
    var i = 0;var max=divs.length;  
    var toTreate = [];
    while (i < max) {
        var div = divs[i++];
        if (div.getAttribute('name')=='rssinclude') toTreate[toTreate.length] = div;
    }
    i=0;max = toTreate.length;
    while (i<max) rssincludeUpdateDiv(toTreate[i++], true);
}
if (window.addEventListener) window.addEventListener( 'load', initRss,false);
else if (window.attachEvent) window.attachEvent( 'onload', initRss);

