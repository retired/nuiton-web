/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
// Register the related commands.
var dialogPath = FCKConfig.PluginsPath + 'rssinclude/fck_rssinclude.html';
var rssincludeDialogCmd = new FCKDialogCommand( FCKLang["DlgRssincludeTitle"], FCKLang["DlgRssincludeTitle"], dialogPath, 480, 470 );
FCKCommands.RegisterCommand( 'rssinclude', rssincludeDialogCmd ) ;

// Create the Rssinclude toolbar button.
var oRssincludeItem		= new FCKToolbarButton( 'rssinclude', FCKLang["DlgRssincludeTitle"]) ;
oRssincludeItem.IconPath	= FCKConfig.PluginsPath + 'rssinclude/button.rss.gif' ;

FCKToolbarItems.RegisterItem( 'rssinclude', oRssincludeItem ) ;
// 'Rssinclude' is the name used in the Toolbar config.

FCK.ContextMenu.RegisterListener( {
	AddItems : function( menu, tag, tagName ) {
		var e = tag;
		if (! (e && e.nodeName == 'DIV' && e.getAttribute('name') == 'rssinclude' )) {
			var selection = FCKSelection.GetSelection() ;
			var range = selection.getRangeAt(0) ;
			e = range.endContainer ;
			while (e && (e.nodeName != 'DIV' || e.getAttribute('name') != 'rssinclude')) {
				e = e.parentNode;
			}
		}

		// under what circumstances do we display this option
		if (!!e &&  e.nodeName == 'DIV' && e.getAttribute('name') == 'rssinclude' ) {
			FCKSelection.SelectNode(e);
			// when the option is displayed, show a separator  the command
			menu.AddSeparator() ;
			// the command needs the registered command name, the title for the context menu, and the icon path
			menu.AddItem( 'rssinclude', FCKLang.DlgRssincludeTitle, oRssincludeItem.IconPath ) ;
		}
	}
} );

/**
OnClick = function( e )
{
	if ( e.target.tagName == 'DIV' && e.target.getAttribute('class') == 'rssinclude' )
	    FCKSelection.SelectNode( e.target ) ;
}

FCK.EditorDocument.addEventListener( 'click', OnClick, true ) ;
**/

OnDoubleClick = function( e ) {
	if ( e.tagName == 'DIV' && e.getAttribute('name') == 'rssinclude' ) {
		FCKSelection.SelectNode(e);
	    FCKCommands.GetCommand( 'rssinclude' ).Execute() ;
	}
}

FCK.RegisterDoubleClickHandler( OnDoubleClick, 'DIV' ) ;
