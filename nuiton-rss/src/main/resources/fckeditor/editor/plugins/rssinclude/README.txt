This plugin was developped by Code Lutin

Installation Instructions
1. Copy rssinclude directory into "editor/plugins/"

2. Copy myfckconfig.js in fckeditor root path and customize it (or merge the 
   content in your already customized config...)

3. To create an editor use this code

var oFCKeditor = new FCKeditor( 'editorName' ) ;    
oFCKeditor.Config["CustomConfigurationsPath"] = oFCKeditor.BasePath+"/myfckconfig.js";
oFCKeditor.Create() ;

Please let me know if you experience any issues.
