/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
// Rssinclude Dialog
FCKLang["DlgRssincludeTitle"] = "Rss Include";
FCKLang["DlgRssincludeFeedName"] = "nom ou URL" ;
FCKLang["DlgRssincludeFeedNameEdit"] = "Edition" ;
FCKLang["DlgRssincludeFeedNameAdd"] = "Ajout" ;
FCKLang["DlgRssincludeNbItem"]   = "nombre d'item" ;
FCKLang["DlgRssincludeFeedRepr"] = "Feed representation" ;
FCKLang["DlgRssincludeAlertFeedName"]      = "Veuillez entrer un nom ou une URL pour le feed"
FCKLang["DlgRssincludeLoading"]      = "Chargement du rss..."
