/*
 * #%L
 * Nuiton Web :: Nuiton RSS
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
var dialog      = window.parent;
var oEditor     = dialog.InnerDialogLoaded();
var FCK         = oEditor.FCK;
var FCKLang     = oEditor.FCKLang;
var FCKConfig   = oEditor.FCKConfig;
var FCKTools    = oEditor.FCKTools;

// Set the language direction.
window.document.dir = oEditor.FCKLang.Dir;

// load rssinclude script
document.writeln("<script type=\"text/javascript\" src=\""+FCKConfig.RssScriptURL+"\"></script>");

// Recuperation du feed selectionne
var oRss = dialog.Selection.GetSelectedElement();

// Un peu de travail au chargement de la page
window.onload = function()
{
    // Translate the dialog box texts.
    oEditor.FCKLanguageManager.TranslatePage(document);
    
    // chargement de la combo avec les feeds configures
    LoadCombo();

    // Load the selected element information (if any).
    LoadSelection();

    // Activate the "OK" button.
    window.parent.SetOkButton( true );

    SelectField( 'txtFeedName' );
}

/**
 * Remplace le textfield de saisie du nom d'un feed par une combo.
 * Cela n'est fait que si le serveur peut etre contacte
 */
function LoadCombo() {
    var xhr = null;
    if(window.XMLHttpRequest) // Firefox
        xhr = new window.XMLHttpRequest();
    else if(window.ActiveXObject) // Internet Explorer
        xhr = new window.ActiveXObject("Microsoft.XMLHTTP");

    if (xhr) {
        xhr.open("GET", FCKConfig.RssKnownFeedsURL, true);
        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4 && xhr.status == 200) /* 200 : code HTTP pour OK */
            {
                var content = '<select style="WIDTH: 100%" id="txtFeedName" onblur="updatePreview(this);">'+ 
                    xhr.responseText + '</select>';
                document.getElementById("divFeedName").innerHTML = content;
                if (oRss) {
                    // on selectionne l'ancienne valeur
                    GetE('txtFeedName').value = GetAttribute( oRss, 'feedname', '' );
                }
            }
        }
        xhr.send(null);
    }
}

/**
 * Chargement du feed que l'on re-edite
 */
function LoadSelection() {
    if (! oRss) return;
    GetE('txtFeedName').value = GetAttribute( oRss, 'feedname', '' );
    GetE('txtNbItem').value   = GetAttribute( oRss, 'nbitem', '' );
    GetE('txtFeedRepr').value = GetAttribute( oRss, 'feedrepr', '' );

    updatePreview();
}

/**
 * Fonction appeler lors de la validation
 */
function Ok()
{
    if ( GetE('txtFeedName').value.length == 0 )
    {
        GetE('txtFeedName').focus();

        alert( oEditor.FCKLang.DlgRssincludeAlertFeedName );
        return false ;
    }
    
    oEditor.FCKUndo.SaveUndoStep();
    if ( !oRss )
    {
        oRss = FCK.InsertElement( 'DIV' ) ;
        if (!detectScript()) {
            // add script to editor only once
            oScript = FCK.InsertElement('SCRIPT');
            SetAttribute( oScript, 'src', FCKConfig.RssScriptURL);
        }        
    }
    updateRss( oRss );
    
    // rssincludeUpdateDiv(oRss, false);
    return true;
}

/**
 * Detection de la presence ou non du script rssinclude dans l'edition
 */
function detectScript() {
    var scripts = FCK.GetXHTML();
    return (scripts.indexOf("src=\""+FCKConfig.RssScriptURL+"\"")!=-1);
}

/**
 * Mise a jour d'un div avec les informations trouvees dans l'interface
 */
function updateRss(e){
    e.contentEditable = false;    
    
    SetAttribute( e, 'class', 'rssinclude');
    SetAttribute( e, 'contentEditable', 'false');
    SetAttribute( e, 'name', 'rssinclude');
    SetAttribute( e, 'feedname', GetE('txtFeedName').value );
    SetAttribute( e, 'nbitem', GetE('txtNbItem').value );
    SetAttribute( e, 'feedrepr', GetE('txtFeedRepr').value );
    //SetAttribute( e, 'forceReload', 'true' );
    e.innerHTML = 'RSS ' + GetE('txtFeedName').value + ' (items:'+GetE('txtNbItem').value+')';
}

// L'element servant a la preview
var ePreview ;

/**
 * Mise a jour du flux dans la preview
 * @param combo la combo utilisee (undefined si uniquement un input)
 */
function updatePreview(combo){

    if ( !ePreview ) {
        ePreview = GetE('rssincludePreview');
    }

    if ( ! ePreview) {
        return;
    }

    if ( GetE('txtFeedName').value.length == 0 ) {
        ePreview.innerHTML = 'Invalid RSS';
    } else {
        if (!!combo) {
            // mise à jour des champs à partir de l'option selectionne
            var index = combo.options.selectedIndex;
            var option = combo.options[index];
            GetE('txtNbItem').value =  GetAttribute( option, 'nbitem', '' )
            GetE('txtFeedRepr').value =  GetAttribute( option, 'feedrepr', '' )
        }
        updateRss(ePreview);
        rssincludeUpdateDiv(ePreview, true);
    }
}
