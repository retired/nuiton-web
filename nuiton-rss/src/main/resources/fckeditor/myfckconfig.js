﻿﻿/*
 * %%Ignore-License
 *
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2008 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * Editor configuration settings.
 *
 * Follow this link for more information:
 * http://wiki.fckeditor.net/Developer%27s_Guide/Configuration/Configurations_Settings
 */

FCKConfig.Plugins.Add("rssinclude", "en,fr");

FCKConfig.ToolbarSets["Default"] = [
	['Source','DocProps','-','Save','NewPage','Preview','-','Templates'],
	['Cut','Copy','Paste','PasteText','PasteWord','-','Print','SpellCheck'],
	['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	['Form','Checkbox','Radio','TextField','Textarea','Select','Button','ImageButton','HiddenField'],
	'/',
	['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
	['OrderedList','UnorderedList','-','Outdent','Indent','Blockquote'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
	['Link','Unlink','Anchor'],
	['rssinclude', 'Image','Flash','Table','Rule','Smiley','SpecialChar','PageBreak'],
	'/',
	['Style','FontFormat','FontName','FontSize'],
	['TextColor','BGColor'],
	['FitWindow','ShowBlocks','-','About']		// No comma for the last row.
] ;

// to authorize user to add a new feed
FCKConfig.RssCanAddFeed = false;
  
// to authorize user to edit a known feed
FCKConfig.RssCanEditFeed = false;
  
// url to render a feed
FCKConfig.RssRenderURL = '/nuitonrss-2.4/RSSServlet&feedName=' ;
  
// url to obtain rssinclude script
FCKConfig.RssScriptURL = '/nuitonrss-2.4/RSSServlet' ;
  
// url to obtain known feeds as a list of html options
FCKConfig.RssKnownFeedsURL = '/nuitonrss-2.4/feeds.txt' ;
  
// url to create a new feed
FCKConfig.RssAddURL = '';

// url to edit a known feed
FCKConfig.RssEditURL = '' ;
