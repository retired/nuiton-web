/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
 * $Id$
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.nuiton.web.struts2.taglib;

import com.opensymphony.xwork2.util.ValueStack;
import org.apache.struts2.components.FieldError;
import org.apache.struts2.views.annotations.StrutsTag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * <!-- START SNIPPET: javadoc -->
 * <p>
 * Render field errors if they exists. Specific layout depends on the particular theme.
 * The field error strings will be html escaped by default.
 * <p>
 * <!-- END SNIPPET: javadoc -->
 * <p>
 * <p> <b>Examples</b>
 * <p>
 * <pre>
 * <!-- START SNIPPET: example -->
 *
 *    &lt;!-- example 1 --&gt;
 *    &lt;sp:fielderror2 /&gt;
 *
 *    &lt;!-- example 2 --&gt;
 *    &lt;sp:fielderror2&gt;
 *         &lt;s:param&gt;field1&lt;/s:param&gt;
 *         &lt;s:param&gt;field2&lt;/s:param&gt;
 *    &lt;/sp:fielderror2&gt;
 *    &lt;s:form .... &gt;
 *       ....
 *    &lt;/s:form&gt;
 *
 *    OR
 *
 *    &lt;sp:fielderror2&gt;
 *          &lt;s:param value="%{'field1'}" /&gt;
 *          &lt;s:param value="%{'field2'}" /&gt;
 *    &lt;/sp:fielderror2&gt;
 *    &lt;s:form .... &gt;
 *       ....
 *    &lt;/s:form&gt;
 *
 *    OR
 *
 *    &lt;sp:fielderror2 fieldName="field1" /&gt;
 *
 * <!-- END SNIPPET: example -->
 * </pre>
 * <p>
 * <p>
 * <p> <b>Description</b><p>
 * <p>
 * <p>
 * <pre>
 * <!-- START SNIPPET: description -->
 *
 * Example 1: display all field errors
 * Example 2: display field errors only for 'field1' and 'field2'
 *
 * <!-- END SNIPPET: description -->
 * </pre>
 */
@StrutsTag(name = "fielderror2", tldTagClass = "org.chorem.pollen.ui.FieldError2Tag", description = "Render field error (all " +
                                                                                                    "or partial depending on param tag nested)if they exists")
public class FieldError2 extends FieldError {


    public FieldError2(ValueStack stack,
                       HttpServletRequest request,
                       HttpServletResponse response) {
        super(stack, request, response);
    }

    @Override
    protected void evaluateExtraParams() {

        List<String> fieldErrorFieldNames = getFieldErrorFieldNames();

        if (fieldErrorFieldNames != null) {

            List<String> valued = new ArrayList<String>();
            for (String errorFieldName : fieldErrorFieldNames) {
                String fieldValue;
                if (errorFieldName.contains("%")) {
                    fieldValue = findString(errorFieldName);
                } else {
                    fieldValue = errorFieldName;
                }
                valued.add(fieldValue);
            }
            fieldErrorFieldNames.clear();
            fieldErrorFieldNames.addAll(valued);
        }

        super.evaluateExtraParams();
    }

}

