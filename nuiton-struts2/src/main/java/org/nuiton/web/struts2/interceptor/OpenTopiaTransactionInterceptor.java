/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.struts2.interceptor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaTransaction;
import org.nuiton.topia.persistence.TopiaTransactionAware;
import org.nuiton.web.struts2.filter.CloseTopiaTransactionFilter;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.opensymphony.xwork2.util.TextParseUtil;

/**
 * <!-- START SNIPPET: description -->
 * <p>
 * The aim of this Interceptor is to manage a {@code transaction} all along
 * a action which implements {@link TopiaTransactionAware} contract.
 * <p>
 * Technicaly, the action will receive only a proxy of a transaction and a real
 * transaction will only be created as soon as a method will be asked on it.
 * <p>
 * The interceptor is abstract and let user to implement the way how to open a
 * new transaction via the method {@link #beginTransaction()}.
 * <p>
 * Note that the transaction pushed in the action can be limited using a list
 * of methods to exclude on it. The list of methods to forbid can be customized
 * using the interceptor parameter {@link #excludeMethods}.
 * <p>
 * Note also that the transaction is <strong>not</strong> commited nor closed.
 * If you want the transaction to be closed, you may use
 * {@link CloseTopiaTransactionFilter} by adding
 * it to your web.xml file.
 * <p>
 * This interceptor, as it provides connection to database should be in the
 * interceptor stack before any other interceptor requiring access to database.
 * For example, it is a common behaviour to do such calls in a prepare method,
 * so make sure to place this interceptor before the {@code prepare} interceptor.
 * <!-- END SNIPPET: description -->
 * <p>
 * <p> <u>Interceptor parameters:</u>
 * <p>
 * <!-- START SNIPPET: parameters -->
 * <p>
 * <ul>
 * <li>excludeMethods (optional) - Customized method names separated by coma to
 * forbid on the proxy of the transaction given to action. By default, if this
 * parameter is not filled, then we will use this one :
 * {@link #DEFAULT_EXCLUDE_METHODS}.</li>
 * </ul>
 * <p>
 * <!-- END SNIPPET: parameters -->
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @author bleny &lt;leny@codelutin.com&gt;
 * @since 1.5
 * @deprecated since 1.6, prefer use the neutral filter org.nuiton.web.filter.TopiaTransactionFilter from nuiton-web module
 */
@Deprecated
public abstract class OpenTopiaTransactionInterceptor extends AbstractInterceptor {

    /** Logger. */
    private static final Log log =
                    LogFactory.getLog(OpenTopiaTransactionInterceptor.class);

    private static final long serialVersionUID = 1L;

    public static final String TOPIA_TRANSACTION_REQUEST_ATTRIBUTE = "topiaTransaction";

    public static final String[] DEFAULT_EXCLUDE_METHODS = {
            "beginTransaction",
            "closeContext",
            "clear"
    };

    /** names of methods to forbid access while using proxy. */
    protected Set<String> excludeMethods;

    public Set<String> getExcludeMethods() {
        return excludeMethods;
    }

    public void setExcludeMethods(String excludeMethods) {
        this.excludeMethods =
                TextParseUtil.commaDelimitedStringToSet(excludeMethods);
    }

    /**
     * Method to open a new transaction.
     *
     * @return the new freshly opened transaction
     * @throws TopiaException if any problem while opening a new transaction
     */
    protected abstract TopiaPersistenceContext beginTransaction() throws TopiaException;

    @Override
    public void init() {
        super.init();

        if (getExcludeMethods() == null) {

            // use default exclude methods
            excludeMethods = new HashSet<String>(
                    Arrays.asList(DEFAULT_EXCLUDE_METHODS)
            );
        }
    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {

        TopiaTransactionAware transactionAware = null;

        Object action = invocation.getProxy().getAction();

        if (action instanceof TopiaTransactionAware) {
            transactionAware = (TopiaTransactionAware) action;
        }

        if (transactionAware == null) {

            // not a transaction aware action, direct skip this interceptor
            return invocation.invoke();
        }

        // creates a proxy of a lazy transaction

        TopiaTransactionProxyInvocationHandler proxyInvocationHandler =
                new TopiaTransactionProxyInvocationHandler();

        TopiaPersistenceContext proxy = (TopiaPersistenceContext) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class<?>[]{TopiaPersistenceContext.class,
                        TopiaTransaction.class},
                proxyInvocationHandler
        );

        // set the transaction in the action
        transactionAware.setTransaction(proxy);

        return invocation.invoke();
    }

    /**
     * Handler of a proxy on a {@link TopiaPersistenceContext}.
     *
     * @see #excludeMethods
     */
    public class TopiaTransactionProxyInvocationHandler implements InvocationHandler {

        /** Target to use for the proxy. */
        protected TopiaPersistenceContext transaction;

        public TopiaPersistenceContext getTransaction() {
            return transaction;
        }

        @Override
        public Object invoke(Object proxy,
                             Method method,
                             Object[] args) throws Throwable {

            String methodName = method.getName();

            if (getExcludeMethods().contains(methodName)) {

                // not authorized
                throw new IllegalAccessException(
                        "Not allowed to access method " + methodName + " on " +
                        proxy);
            }

            if (transaction == null) {

                if (log.isTraceEnabled()) {
                    log.trace("transaction started due to a call to " + methodName);
                }

                // first time transaction is required, create its
                transaction = beginTransaction();

                // push transaction in request to make it available for closing in
                // CloseTopiaTransactionFilter
                ServletActionContext.getRequest().setAttribute(
                        TOPIA_TRANSACTION_REQUEST_ATTRIBUTE, transaction);

                if (log.isDebugEnabled()) {
                    log.debug("Open transaction " + transaction);
                }
            }

            // can invoke the method on the transaction
            try {
                Object result = method.invoke(transaction, args);
                return result;
            } catch (Exception eee) {
                if (log.isErrorEnabled()) {
                    log.error("Could not execute method " + method.getName(), eee);
                }
                throw eee;
            }
        }
    }
}
