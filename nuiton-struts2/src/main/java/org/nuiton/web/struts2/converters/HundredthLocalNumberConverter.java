package org.nuiton.web.struts2.converters;

/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Same as of {@link LocalNumberConverter} but always use 2 decimals: it's
 * a suitable default for money, etc.
 *
 * @since 1.15
 */
public class HundredthLocalNumberConverter extends LocalNumberConverter {

    @Override
    protected int getMinDecimals () {
        return 2;
    }

    @Override
    protected int getMaxDecimals () {
        return 2;
    }

}
