package org.nuiton.web.struts2;

/*-
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.StrutsTextProviderFactory;
import com.opensymphony.xwork2.TextProvider;
import org.apache.struts2.StrutsConstants;

import java.util.ResourceBundle;

/**
 * May be declared as {@link StrutsConstants#STRUTS_TEXT_PROVIDER_FACTORY} with
 *
 * <pre>
 *       &lt;bean class=&quot;org.nuiton.web.struts2.I18nTextProviderFactory&quot; name=&quot;i18nTextProviderFactory&quot; type=&quot;com.opensymphony.xwork2.TextProviderFactory&quot; /&gt;
 *       &lt;constant name=&quot;struts.textProviderFactory&quot; value=&quot;i18nTextProviderFactory&quot; /&gt;
 * </pre>
 *
 * @since 1.20
 * @deprecated since struts 2.5.14.1 a {@link TextProvider} can be provided using default factory. See https://issues.apache.org/jira/browse/WW-4884
 */
@Deprecated
public class I18nTextProviderFactory extends StrutsTextProviderFactory {

    @Override
    protected TextProvider getTextProvider(Class clazz) {
        return new I18nTextProvider();
    }

    @Override
    protected TextProvider getTextProvider(ResourceBundle bundle) {
        return new I18nTextProvider();
    }
}
