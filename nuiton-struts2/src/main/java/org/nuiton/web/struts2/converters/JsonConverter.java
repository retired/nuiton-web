package org.nuiton.web.struts2.converters;

/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.json.DefaultJSONWriter;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONPopulator;
import org.apache.struts2.json.JSONUtil;
import org.apache.struts2.json.JSONWriter;
import org.apache.struts2.util.StrutsTypeConverter;

import java.util.Arrays;
import java.util.Map;

/**
 * This converter will convert any field by serializing it to its JSON String representation.
 *
 * You must have struts2-json-plugin provided in you classpath.
 *
 * If you want the serialized JSON to be printed properly in a JSP, don't forget to escape it:
 *
 * <pre>
 *     &lt;s:property value="my.path.to.myProperty" escapeHtml="false" /&gt;
 * </pre>
 */
public class JsonConverter extends StrutsTypeConverter {

    private static final Log log = LogFactory.getLog(JsonConverter.class);

    @Override
    public Object convertFromString(Map context, String[] values, Class toClass) {

        if (log.isTraceEnabled()) {
            log.trace("will convertFromString " + Arrays.toString(values));
        }

        Object object;
        if (values.length == 0) {
            object = null;
        } else if (values.length == 1) {
            String json = values[0];
            if (log.isTraceEnabled()) {
                log.trace("will deserialize " + json);
            }
            try {
                Map deserialize = (Map) JSONUtil.deserialize(json);
                JSONPopulator jsonPopulator = new JSONPopulator();
                object = toClass.newInstance();
                jsonPopulator.populateObject(object, deserialize);
            } catch (Throwable e) {
                if (log.isErrorEnabled()) {
                    log.error("unable to parse json =\n" + json, e);
                }
                throw new RuntimeException("unable to parse json = " + json, e);
            }
        } else {
            throw new UnsupportedOperationException("unable to parse multiple values " + Arrays.toString(values));
        }

        if (log.isTraceEnabled()) {
            log.trace("convert from string " + Arrays.toString(values) + " will return " + object);
        }

        return object;
    }

    @Override
    public String convertToString(Map context, Object object) {

        if (log.isTraceEnabled()) {
            log.trace("will serialize " + object);
        }

        String json;

        if (object == null) {
            json = "null";
        } else {
            try {
                JSONWriter writer = new DefaultJSONWriter();
                json = writer.write(object);
            } catch (JSONException e) {
                if (log.isErrorEnabled()) {
                    log.error("unable to serialize object " + object, e);
                }
                throw new RuntimeException("unable to serialize object " + object, e);
            } catch (Throwable t) {
                if (log.isErrorEnabled()) {
                    log.error("error while serializing object " + object, t);
                }
                throw new RuntimeException("error while serializing object " + object, t);
            }
        }

        if (log.isTraceEnabled()) {
            log.trace("serialize " + object + " will return " + json);
        }

        return json;

    }
}
