/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.struts2.taglib;

import com.opensymphony.xwork2.util.ValueStack;
import org.apache.struts2.components.ClosingUIBean;
import org.apache.struts2.views.annotations.StrutsTag;
import org.apache.struts2.views.annotations.StrutsTagAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@StrutsTag(
    name="help",
    tldTagClass="org.nuiton.web.struts2.taglib.HelpTag",
    description="display an help message for the fields inside this tag",
    allowDynamicAttributes=false)
public class HelpBean extends ClosingUIBean {

    protected static final String TEMPLATE = "help";

    protected static final String TEMPLATE_CLOSE = "help-close";

    protected static final String DEFAULT_HELP_ZONE = "helpZone";

    protected String helpKey;

    protected String helpZone;

    public HelpBean(ValueStack stack, HttpServletRequest req, HttpServletResponse res) {
        super(stack, req, res);
    }

    @Override
    public String getDefaultOpenTemplate() {
        return TEMPLATE;
    }

    @Override
    protected String getDefaultTemplate() {
        return TEMPLATE_CLOSE;
    }

    @Override
    protected void evaluateExtraParams() {
        addParameter("helpKey", helpKey);
        addParameter("helpZone", helpZone == null ? DEFAULT_HELP_ZONE : helpZone);
    }

    @StrutsTagAttribute(description="the key used to get the help message body", required = true)
    public void setHelpKey(String helpKey) {
        this.helpKey = helpKey;
    }

    @StrutsTagAttribute(description="the id of the element where the help message will be pushed")
    public void setHelpZone(String helpZone) {
        this.helpZone = helpZone;
    }
}
