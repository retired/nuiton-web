/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.struts2.filter;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.web.struts2.interceptor.OpenTopiaTransactionInterceptor;

/** Close a TopiaTransaction instance stored in the servlet context.
 *
 * The main usage is to close a transaction opened by the
 * {@link OpenTopiaTransactionInterceptor}. A convention is used to find it,
 * the transaction attached to the request is found at key
 * {@link OpenTopiaTransactionInterceptor#TOPIA_TRANSACTION_REQUEST_ATTRIBUTE}
 *
 * @since 1.5
 * @author bleny
 * @deprecated since 1.6, prefer use the neutral filter org.nuiton.web.filter.TopiaTransactionFilter from nuiton-web module
 */
@Deprecated
public class CloseTopiaTransactionFilter implements Filter {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CloseTopiaTransactionFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            TopiaPersistenceContext transaction = (TopiaPersistenceContext)
                    servletRequest.getAttribute(
                            OpenTopiaTransactionInterceptor.TOPIA_TRANSACTION_REQUEST_ATTRIBUTE);

            if (transaction == null) {
                if (log.isTraceEnabled()) {
                    log.trace("no transaction to close");
                }
            } else if (transaction.isClosed()) {
                if (log.isTraceEnabled()) {
                    log.trace("transaction " + transaction + " is already closed");
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("close transaction " + transaction);
                }
                transaction.close();
            }
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nothing to do
    }

    @Override
    public void destroy() {
        // nothing to do
    }
}
