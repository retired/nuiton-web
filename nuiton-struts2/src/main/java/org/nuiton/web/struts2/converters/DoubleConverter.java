package org.nuiton.web.struts2.converters;

/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.conversion.TypeConversionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.util.StrutsTypeConverter;

import java.util.Arrays;
import java.util.Map;

/**
 * Let user input either "0.1" or "0,1" as a double value. To use this converter, add the following lines in your
 * <code>classpath:xwork-conversion.properties</code> file :
 *
 * <pre>
 * java.lang.Double=org.nuiton.web.struts2.converters.DoubleConverter
 * double=org.nuiton.web.struts2.converters.DoubleConverter
 * </pre>
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 * @deprecated since 1.15 prefer use LocalNumberConverter
 */

@Deprecated
public class DoubleConverter extends StrutsTypeConverter {

    @Override
    public Object convertFromString(Map map, String[] strings, Class aClass) {
        if (strings.length == 1) {
            String string = strings[0];
            string = string.replace(",", ".");

            Double parsedValue;
            if (string == null || string.isEmpty()) {
                if (double.class.equals(aClass)) {
                    parsedValue = 0.;
                } else {
                    parsedValue = null;
                }
            } else {
                parsedValue = Double.parseDouble(string);
            }
            return parsedValue;
        } else {
            throw new TypeConversionException("strings=" + Arrays.toString(strings));
        }
    }

    @Override
    public String convertToString(Map map, Object o) {
        Double d = (Double) o;

        String string;
        if (d == null) {
            string = StringUtils.EMPTY;
        } else {
            string = d.toString();
        }

        return string;
    }

}
