<#--
 #%L
 Nuiton Web :: Nuiton Struts 2
 %%
 Copyright (C) 2010 - 2011 CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->
<div id="idHelpKey${parameters.helpKey}">
    <!-- Note : Your page need to implement the way doc will be retrieved through a "pushHelpToHtml" method-->
    <script type="text/javascript">
        $(document).ready( function() {
            var helpZone = $('#${parameters.helpZone}');
            var initialHtml;

            function pushDocToHelpZone(docTitle, docContent) {
                if (!docTitle && !docContent) {
                    hideHelp();
                } else {
                    var result =
                        "<dl>" + docTitle + "</dl>" +
                        "<dd>" + docContent + "</dd>";
                    helpZone.html(result);
                }
            }

            function showHelp() {
                initialHtml = helpZone.html();
                // User must define its own "pushHelpToHtml" method
                pushHelpToHtml("${parameters.helpKey}", pushDocToHelpZone);
            }
            function hideHelp() {
                helpZone.html(initialHtml);
            }

            /* select, text, text areas and password fields */
            $('#idHelpKey${parameters.helpKey} input, #idHelpKey${parameters.helpKey} select, #idHelpKey${parameters.helpKey} textarea').focus(showHelp);
            $('#idHelpKey${parameters.helpKey} input, #idHelpKey${parameters.helpKey} select, #idHelpKey${parameters.helpKey} textarea').blur(hideHelp);

            /* radio buttons, check-boxes */
            var fieldsNeedingHelpWhenMouseOver =
                    $('#idHelpKey${parameters.helpKey} input[type=radio], #idHelpKey${parameters.helpKey} input[type=checkbox], #idHelpKey${parameters.helpKey} input[type=submit], #idHelpKey${parameters.helpKey} label, #idHelpKey${parameters.helpKey} select');
            fieldsNeedingHelpWhenMouseOver.mouseover(showHelp);
            fieldsNeedingHelpWhenMouseOver.mouseout(hideHelp);
        });
    </script>
