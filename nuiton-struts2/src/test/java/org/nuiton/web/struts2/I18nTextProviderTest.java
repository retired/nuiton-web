/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.struts2;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;

import java.util.Arrays;
import java.util.Locale;

public class I18nTextProviderTest {

    protected I18nTextProvider provider;

    @After
    public void after() {
        I18n.close();
    }

    @Before
    public void init() {

        DefaultI18nInitializer initializer = new DefaultI18nInitializer("toto");
        initializer.setMissingKeyReturnNull(true);
        I18n.init(initializer, Locale.FRENCH);

        provider = new I18nTextProvider() {
            @Override
            protected Locale getLocale() {
                return Locale.FRENCH;
            }
        };

    }

    @Test
    public void testHasKey() {
        Assert.assertFalse(provider.hasKey("missing"));
        Assert.assertTrue(provider.hasKey("present"));
    }

    @Test
    public void testGetTextSimple() {
        Assert.assertEquals("yes", provider.getText("present"));
        Assert.assertEquals(I18nTextProvider.UNTRANSLATED_MARKER + "missing" + I18nTextProvider.UNTRANSLATED_MARKER, provider.getText("missing"));
    }

    @Test
    public void testGetTextDefaultValue() {
        Assert.assertEquals("yes", provider.getText("present", "toto"));
        Assert.assertEquals("toto", provider.getText("missing", "toto"));
    }

    @Test
    public void testGetTextArg() {
        Assert.assertEquals("Hello Arno !", provider.getText("hello", null, "Arno"));
    }

    @Test
    public void testGetTextArgs() {
        Assert.assertEquals("Hello Arno, ça biche ?", provider.getText("hello_plus", new String[] {"Arno", "ça biche"}));
        Assert.assertEquals("Hello Moto, ça gaze ?", provider.getText("hello_plus", Arrays.asList("Moto", "ça gaze")));

        Assert.assertEquals("Bonjour", provider.getText("hello_moins", "Bonjour", new String[] {"Arno", "ça biche"}));
        Assert.assertEquals("Bonjour", provider.getText("hello_moins", "Bonjour", Arrays.asList("Moto", "ça gaze")));
    }

}
