package org.nuiton.web.struts2.converters;

/*
 * #%L
 * Nuiton Web :: Nuiton Struts 2
 * %%
 * Copyright (C) 2010 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * @author Sylvain Bavencoff &lt;bavencoff@codelutin.com&gt;
 */
public class LocalNumberConverterTest {

    protected LocalNumberConverter converter;

    protected Object convert(Object value, Class toType) {
        return converter.convertValue(null, null, null, "", value, toType);
    }

    @Test
    public void testConvertValue() {

        converter = new LocalNumberConverter();

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());

        symbols.getCurrency();
        char minusSign = symbols.getMinusSign();
        char groupingSeparator = symbols.getGroupingSeparator();
        char decimalSeparator = symbols.getDecimalSeparator();


        // Integer to String
        Assert.assertEquals("0",
                convert(0, String.class));

        Assert.assertEquals("0",
                convert(0, String.class));

        Assert.assertEquals("18",
                convert(18, String.class));

        Assert.assertEquals("18",
                convert(18, String.class), "18");

        Assert.assertEquals(minusSign + "18",
                convert(-18, String.class));

        Assert.assertEquals(minusSign + "18",
                convert(-18, String.class));

        Assert.assertEquals("1" + groupingSeparator + "000" + groupingSeparator + "008",
                convert(1000008, String.class));

        Assert.assertEquals("1" + groupingSeparator + "000" + groupingSeparator + "008",
                convert(1000008, String.class));

        Assert.assertEquals(minusSign + "1" + groupingSeparator + "000" + groupingSeparator + "008",
                convert(-1000008, String.class));

        Assert.assertEquals(minusSign + "1" + groupingSeparator + "000" + groupingSeparator + "008",
                convert(-1000008, String.class));

        // Double to String
        Assert.assertEquals("0",
                convert(0, String.class));

        Assert.assertEquals("0",
                convert((double) 0, String.class));

        Assert.assertEquals("18",
                convert(18, String.class));

        Assert.assertEquals("18", convert((double) 18, String.class));

        Assert.assertEquals(minusSign + "18",
                convert(-18, String.class));

        Assert.assertEquals(minusSign + "18",
                convert((double) -18, String.class));

        Assert.assertEquals("1" + groupingSeparator + "000" + groupingSeparator + "008",
                convert(1000008, String.class));

        Assert.assertEquals("1" + groupingSeparator + "000" + groupingSeparator + "008",
                convert((double) 1000008, String.class));

        Assert.assertEquals(minusSign + "1" + groupingSeparator + "000" + groupingSeparator + "008",
                convert(-1000008, String.class));

        Assert.assertEquals(minusSign + "1" + groupingSeparator + "000" + groupingSeparator + "008",
                convert((double) -1000008, String.class));

        Assert.assertEquals("0" + decimalSeparator + "58",
                convert(0.58, String.class));

        Assert.assertEquals("0" + decimalSeparator + "58",
                convert(0.58, String.class));

        Assert.assertEquals("18" + decimalSeparator + "58",
                convert(18.58, String.class));

        Assert.assertEquals("18" + decimalSeparator + "58",
                convert(18.58, String.class));

        Assert.assertEquals(minusSign + "18" + decimalSeparator + "58",
                convert(-18.58, String.class));

        Assert.assertEquals(minusSign + "18" + decimalSeparator + "58",
                convert(-18.58, String.class));

        Assert.assertEquals("1" + groupingSeparator + "000" + groupingSeparator + "008" + decimalSeparator + "58",
                convert(1000008.58, String.class));

        Assert.assertEquals("1" + groupingSeparator + "000" + groupingSeparator + "008" + decimalSeparator + "58",
                convert(1000008.58, String.class));

        Assert.assertEquals(minusSign + "1" + groupingSeparator + "000" + groupingSeparator + "008" + decimalSeparator + "58",
                convert(-1000008.58, String.class));

        Assert.assertEquals(minusSign + "1" + groupingSeparator + "000" + groupingSeparator + "008" + decimalSeparator + "58",
                convert(-1000008.58, String.class));

        Assert.assertEquals("0" + decimalSeparator + "586",
                convert(0.5864, String.class));

        Assert.assertEquals("0" + decimalSeparator + "586",
                convert(0.5864, String.class));

        Assert.assertEquals("0" + decimalSeparator + "587",
                convert(0.5867, String.class));

        Assert.assertEquals("0" + decimalSeparator + "587",
                convert(0.5867, String.class));

        // String to Integer
        Assert.assertEquals(0,
                convert(null, int.class));

        Assert.assertEquals(null,
                convert(null, Integer.class));

        Assert.assertEquals(0,
                convert("", int.class));

        Assert.assertEquals(null,
                convert("", Integer.class));

        Assert.assertEquals(0,
                convert("0", int.class));

        Assert.assertEquals(0,
                convert("0", Integer.class));

        Assert.assertEquals(12,
                convert("12", int.class));

        Assert.assertEquals(12,
                convert("12", Integer.class));

        Assert.assertEquals(-12,
                convert(minusSign + "12", int.class));

        Assert.assertEquals(-12,
                convert(minusSign + "12", Integer.class));

        if (Character.isSpaceChar(groupingSeparator)) {

            Assert.assertEquals(1002,
                    convert("1 002", int.class));

            Assert.assertEquals(1002,
                    convert("1 002", Integer.class));

            Assert.assertEquals(-1002,
                    convert(minusSign + "1 002", int.class));

            Assert.assertEquals(-1002,
                    convert(minusSign + "1 002", Integer.class));

        }

        Assert.assertEquals(1002,
                convert("1" + groupingSeparator + "002", int.class));
        Assert.assertEquals(1002,
                convert("1" + groupingSeparator + "002", Integer.class));

        Assert.assertEquals(-1002,
                convert(minusSign + "1" + groupingSeparator + "002", int.class));

        Assert.assertEquals(-1002,
                convert(minusSign + "1" + groupingSeparator + "002", Integer.class));

        // String to Double
        Assert.assertEquals(0d,
                convert(null, double.class));

        Assert.assertEquals(null,
                convert(null, Double.class));

        Assert.assertEquals(0d,
                convert("", double.class));

        Assert.assertEquals(null,
                convert("", Double.class));

        Assert.assertEquals(0d,
                convert("0", double.class));

        Assert.assertEquals((double) 0,
                convert("0", Double.class));

        Assert.assertEquals(12d,
                convert("12", double.class));

        Assert.assertEquals((double) 12,
                convert("12", Double.class));

        Assert.assertEquals(-12d,
                convert(minusSign + "12", double.class));

        Assert.assertEquals((double) -12,
                convert(minusSign + "12", Double.class));


        if (Character.isSpaceChar(groupingSeparator)) {

            Assert.assertEquals(1002d,
                    convert("1 002", double.class));

            Assert.assertEquals((double) 1002,
                    convert("1 002", Double.class));

            Assert.assertEquals(-1002d,
                    convert(minusSign + "1 002", double.class));

            Assert.assertEquals((double) -1002,
                    convert(minusSign + "1 002", Double.class));

        }

        Assert.assertEquals(1002d,
                convert("1" + groupingSeparator + "002", double.class));

        Assert.assertEquals((double) 1002,
                convert("1" + groupingSeparator + "002", Double.class));

        Assert.assertEquals(-1002d,
                convert(minusSign + "1" + groupingSeparator + "002", double.class));

        Assert.assertEquals((double) -1002,
                convert(minusSign + "1" + groupingSeparator + "002", Double.class));

        Assert.assertEquals(0.36,
                convert("0" + decimalSeparator + "36", double.class));

        Assert.assertEquals(0.36,
                convert("0" + decimalSeparator + "36", Double.class));

        Assert.assertEquals(12.36,
                convert("12" + decimalSeparator + "36", double.class));

        Assert.assertEquals(12.36,
                convert("12" + decimalSeparator + "36", Double.class));

        Assert.assertEquals(-12.36,
                convert(minusSign + "12" + decimalSeparator + "36", double.class));

        Assert.assertEquals(-12.36,
                convert(minusSign + "12" + decimalSeparator + "36", Double.class));


        if (Character.isSpaceChar(groupingSeparator)) {

            Assert.assertEquals(1002.36,
                    convert("1 002" + decimalSeparator + "36", double.class));

            Assert.assertEquals(1002.36,
                    convert("1 002" + decimalSeparator + "36", Double.class));

            Assert.assertEquals(-1002.36,
                    convert(minusSign + "1 002" + decimalSeparator + "36", double.class));

            Assert.assertEquals(-1002.36,
                    convert(minusSign + "1 002" + decimalSeparator + "36", Double.class));

        }

        Assert.assertEquals(1002.36,
                convert("1" + groupingSeparator + "002" + decimalSeparator + "36", double.class));

        Assert.assertEquals(1002.36,
                convert("1" + groupingSeparator + "002" + decimalSeparator + "36", Double.class));

        Assert.assertEquals(-1002.36,
                convert(minusSign + "1" + groupingSeparator + "002" + decimalSeparator + "36", double.class));

        Assert.assertEquals(-1002.36,
                convert(minusSign + "1" + groupingSeparator + "002" + decimalSeparator + "36", Double.class));

        Assert.assertEquals(12.3669,
                convert("12" + decimalSeparator + "3669", double.class));

        Assert.assertEquals(12.3669,
                convert("12" + decimalSeparator + "3669", Double.class));

        Assert.assertEquals(12.36,
                convert("12.36", double.class));

        Assert.assertEquals(12.36,
                convert("12.36", Double.class));

        if (Character.isSpaceChar(groupingSeparator)) {

            Assert.assertEquals(1002.36,
                                convert("1 002.36", double.class));

            Assert.assertEquals(1002.36,
                                convert("1 002.36", Double.class));
        }
        Assert.assertEquals(1002.36,
                convert("1002.36", double.class));

        Assert.assertEquals(1002.36,
                convert("1002.36", Double.class));

    }


}
