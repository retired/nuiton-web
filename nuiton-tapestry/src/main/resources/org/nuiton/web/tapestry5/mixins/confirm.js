/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * Dialogue de confirmation
 */
var Confirm = Class.create({
    /**
     * element : Element DOM depuis lequel le dialogue de confirmation va s'ouvrir
     * message : Message à afficher dans la boîte de dialogue
     */
    initialize: function(element, message, condition) {
        this.message = message;
        this.condition = condition;
        Event.observe($(element), 'click', this.doConfirm.bindAsEventListener(this));
    },
    doConfirm: function(e) {
        //Tapestry.debug('click ' + element);
        if (this.condition) {
            if (!confirm(this.message)) {
                //Tapestry.debug('STOP ' + e);
                Event.stop(e);
            }
        }
    }
});
