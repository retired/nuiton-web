/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.tapestry5.data;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.tapestry5.beaneditor.PropertyModel;
import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * AbstractMappedGridDataSource
 * <p>
 * TODO : javadoc
 * <p>
 * Created: 18 janv. 2010
 *
 * @param <K> Type of the map key
 * @param <E> Type of the map value
 * @author fdesbois &lt;fdesbois@codelutin.com&gt;
 * @version $Id$
 */
public abstract class AbstractMappedGridDataSource<K, E>
        implements GridDataSource {

    private Logger log =
            LoggerFactory.getLogger(AbstractMappedGridDataSource.class);

    private Map<K, E> mapResults;

    private List<E> listResults;

    private int nbRows = -1;

    private int nbRowsPerPage;

    private boolean prepared;

    /**
     * Need to be provided by subclass, used by Grid component *
     */
    @Override
    public abstract Class<?> getRowType();

    protected abstract int count();

    protected abstract Map<K, E> execute(int startIndex, int endIndex,
                                         SortConstraint orderBy);

    @Override
    public int getAvailableRows() {
        if (nbRows < 0) {
            nbRows = count();
            if (log.isDebugEnabled()) {
                log.debug("Count : " + nbRows);
            }
        }
        return nbRows;
    }

    @Override
    public void prepare(int startIndex, int endIndex,
                        List<SortConstraint> sortConstraints) {
        if (log.isDebugEnabled()) {
            log.debug("Prepare results : " + startIndex + ", " + endIndex);
        }
        nbRowsPerPage = endIndex - startIndex + 1;
        mapResults = execute(startIndex, endIndex,
                getSortConstraint(sortConstraints));
        listResults = new ArrayList<E>(mapResults.values());
        prepared = true;
    }

    @Override
    public Object getRowValue(int index) {
        index = index % nbRowsPerPage;
        if (index >= listResults.size()) {
            if (log.isErrorEnabled()) {
                log.error("Size error : " + index + " / " + listResults.size());
            }
            return null;
        }
        return CollectionUtils.get(listResults, index);
    }

    /**
     * Detect if the data has already been prepared. This will prevent errors
     * on calling {@link #get(Object)}, {@link #values()} and
     * {@link #contains(Object)} methods.
     *
     * @return true if data is ready, false otherwise.
     */
    public boolean isPrepared() {
        return prepared;
    }

    public E get(K key) {
        if (mapResults == null) {
            return null;
        }
        return mapResults.get(key);
    }

    public List<E> values() {
        if (listResults == null) {
            listResults = new ArrayList<E>();
        }
        return listResults;
    }

    public boolean contains(K key) {
        if (mapResults == null) {
            return false;
        }
        return mapResults.containsKey(key);
    }

    protected SortConstraint getSortConstraint(
            List<SortConstraint> sortConstraints) {
        for (SortConstraint constraint : sortConstraints) {
            final ColumnSort sort = constraint.getColumnSort();
            if (sort != ColumnSort.UNSORTED) {
                return constraint;
            }
        }
        return null;
    }

    protected String resolveOrderBy(SortConstraint orderBy) {
        String filterOrder = null;
        if (orderBy != null) {
            PropertyModel property = orderBy.getPropertyModel();
            filterOrder = property.getPropertyName();

            ColumnSort sort = orderBy.getColumnSort();
            if (sort.equals(ColumnSort.DESCENDING)) {
                filterOrder += " desc";
            }

            if (log.isDebugEnabled()) {
                log.debug("Order : " + filterOrder);
            }
        }
        return filterOrder;
    }

}
