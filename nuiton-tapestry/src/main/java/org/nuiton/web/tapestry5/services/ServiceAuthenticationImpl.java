/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.tapestry5.services;

import org.apache.tapestry5.ioc.internal.util.TapestryException;
import org.apache.tapestry5.services.ApplicationStateManager;

import java.lang.annotation.Annotation;

/**
 * This class is an abstract implementation of {@link ServiceAuthentication}.
 * This service use {@link ApplicationStateManager} to manage connected user.
 * You can inherit this class to provide a momre specific authentication
 * managmentan depends on user type and annotation used to identify
 * pages that need authentication. This behavior is done in
 * {@link #isAllowed(Class)} that detect the annotation on the page (ex :
 * {@link org.nuiton.web.tapestry5.annotations.RequiresAuthentication} annotation) and call the method
 * {@link #checkAuthorizations(Object, Annotation)} that is implemented by
 * default to do nothing (return true).
 * <br>
 * <h2>AppModule contribution :</h2>
 * Exemple with MyUser type for user and RequiresAnnotation for annotation :
 * <pre>
 *  public ServiceAuthentication&lt;MyUser&gt; buildServiceAuthentication(ApplicationStateManager stateManager) {
 *      return new ServiceAuthenticationImpl&lt;MyUser, RequiresAuthentication&gt;(stateManager);
 *  }
 * </pre>
 * No need to contribute to ApplicationStateManager to provide instantiation
 * of user. Even this service used the default constructor to instanciate a new
 * user.
 * <p>
 * Created: 3 mai 2010
 *
 * @param <U> user type
 * @param <A> annotation type used to identify which page need authentication
 * @author fdesbois
 * @version $Id$
 */
public class ServiceAuthenticationImpl<U, A extends Annotation>
        implements ServiceAuthentication<U> {

    /**
     * Tapestry service used to save user in session and retrieve it when
     * needed.
     */
    private final ApplicationStateManager stateManager;

    /**
     * User type
     */
    protected Class<U> userClass;

    /**
     * Annotation type to detect pages that need authentication (ex :
     * RequiresAuthentication).
     */
    protected Class<A> annotationClass;

    /**
     * Constructor to call in subclasses.
     *
     * @param stateManager    from Tapestry
     * @param userClass       to identify which type of user is managed
     * @param annotationClass to identify which type of annotation is managed
     *                        for authentication treatment.
     */
    public ServiceAuthenticationImpl(ApplicationStateManager stateManager,
                                     Class<U> userClass, Class<A> annotationClass) {
        this.stateManager = stateManager;
        this.userClass = userClass;
        this.annotationClass = annotationClass;
    }

    @Override
    public boolean isUserConnected() {
        return stateManager.exists(userClass);
    }

    @Override
    public U getUserConnected() {
        return stateManager.getIfExists(userClass);
    }

    @Override
    public void setUserConnected(U user) {
        stateManager.set(userClass, user);
    }

    /**
     * Detect the annotation in the given {@code page}. If annotation is
     * detected, the user authorizations need to be check using
     * {@link #checkAuthorizations(Object, Annotation)} method.
     *
     * @param page to check
     * @return true if connected user can display the page, false otherwise
     */
    @Override
    public boolean isAllowed(Class<?> page) {
        if (page.isAnnotationPresent(annotationClass) &&
                isUserConnected()) {

            A check = page.getAnnotation(annotationClass);

            U user = getUserConnected();

            return checkAuthorizations(user, check);
        }
        // No restriction if annotation is not present
        return true;
    }

    /**
     * Check the connected user authorizations. Called in
     * {@link #isAllowed(Class)} method after detect the annotation on the
     * current page. Override this method to provide more specific
     * authentication managment.
     *
     * @param user       current user connected.
     * @param annotation from page to check authorizations.
     * @return true if the user is granted, false otherwise
     */
    protected boolean checkAuthorizations(U user, A annotation) {
        // The user is connected so, he can access to the page.
        return true;
    }

    /**
     * Default instanciation of the user. The user class need a default
     * constructor.
     *
     * @return a new user instance.
     */
    @Override
    public U getNewUserInstance() {
        try {
            return userClass.newInstance();
        } catch (Exception eee) {
            new TapestryException("Error on user instanciation " +
                    userClass.getName(), eee);
        }
        return null;
    }

}
