/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.tapestry5.services;

import org.apache.tapestry5.Link;
import org.apache.tapestry5.runtime.Component;
import org.apache.tapestry5.services.ComponentEventRequestParameters;
import org.apache.tapestry5.services.ComponentRequestFilter;
import org.apache.tapestry5.services.ComponentRequestHandler;
import org.apache.tapestry5.services.ComponentSource;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.apache.tapestry5.services.PageRenderRequestParameters;
import org.apache.tapestry5.services.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;

/**
 * AbstractAuthenticationFilter
 * <p>
 * Created: 3 mai 2010
 *
 * @param <U> type of User
 * @param <A> type of Annotation that manage authentication on pages/components
 * @author fdesbois
 * @version $Id$
 */
public abstract class AbstractAuthenticationFilter<U, A extends Annotation>
        implements ComponentRequestFilter {

    private static final Logger logger =
            LoggerFactory.getLogger(AbstractAuthenticationFilter.class);

    private final PageRenderLinkSource pageRender;

    private final ComponentSource componentSource;

    private final Response response;

    private final ServiceAuthentication<U> serviceAuthentication;

    protected Class<A> annotationClass;

    protected enum AuthenticationRedirect {
        NOT_CONNECTED, NOT_ALLOWED;
    }

    protected abstract Class<?> getRedirectPage(
            AuthenticationRedirect redirectType);

    public AbstractAuthenticationFilter(PageRenderLinkSource renderLinkSource,
                                        ComponentSource componentSource, Response response,
                                        ServiceAuthentication<U> serviceAuthentication,
                                        Class<A> annotationClass) {
        this.pageRender = renderLinkSource;
        this.componentSource = componentSource;
        this.response = response;
        this.serviceAuthentication = serviceAuthentication;
        this.annotationClass = annotationClass;

        if (logger.isTraceEnabled()) {
            logger.trace("Construct");
        }
    }

    @Override
    public void handleComponentEvent(
            ComponentEventRequestParameters parameters,
            ComponentRequestHandler handler) throws IOException {

        if (logger.isTraceEnabled()) {
            logger.trace("handleComponentEvent");
        }

        if (redirectUnauthorizedUser(parameters.getActivePageName())) {
            return;
        }

        handler.handleComponentEvent(parameters);

    }

    @Override
    public void handlePageRender(PageRenderRequestParameters parameters,
                                 ComponentRequestHandler handler) throws IOException {

        if (logger.isTraceEnabled()) {
            logger.trace("handlePageRender");
        }

        if (redirectUnauthorizedUser(parameters.getLogicalPageName())) {
            return;
        }

        handler.handlePageRender(parameters);
    }

    protected boolean redirectUnauthorizedUser(String pageName)
            throws IOException {

        Component page = componentSource.getPage(pageName);

        if (logger.isTraceEnabled()) {
            logger.trace("Page name : " + pageName);
            logger.trace("Page class : " + page.getClass());
            logger.trace("RequiresLogin annotation : " +
                    page.getClass().isAnnotationPresent(annotationClass));
            logger.trace("User in session : " +
                    serviceAuthentication.isUserConnected());
        }

        if (!page.getClass().isAnnotationPresent(annotationClass)) {
            return false;
        }

        AuthenticationRedirect redirectType = AuthenticationRedirect.NOT_CONNECTED;

        if (serviceAuthentication.isUserConnected()) {
            U user = serviceAuthentication.getUserConnected();
            if (logger.isTraceEnabled()) {
                logger.trace("User connected : " + user);
                logger.trace("User allowed : " +
                        serviceAuthentication.isAllowed(page.getClass()));
            }

            if (serviceAuthentication.isAllowed(page.getClass())) {
                return false;
            }

            redirectType = AuthenticationRedirect.NOT_ALLOWED;
        }

        Class<?> redirectPage = getRedirectPage(redirectType);
        Link link = pageRender.createPageRenderLinkWithContext(redirectPage,
                pageName);

        if (logger.isTraceEnabled()) {
            logger.trace("Redirection to " + redirectPage.getSimpleName() +
                    " page...");
        }

        response.sendRedirect(link);

        return true;
    }
}

