/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/* *##%
 * Pollen :: Pollen Web Interface
 * Copyright (C) 2009 - 2010 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * ##%*/

package org.nuiton.web.tapestry5.components;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Log;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.ioc.annotations.Inject;

import java.util.ArrayList;
import java.util.List;

/**
 * Composant qui affiche des messages à l'utilisateur.
 * <p>
 * Ce composant permet d'afficher des messages avec une apparence différente
 * selon leur type (info ou erreur). Lors de l'affichage de la page, les
 * messages seront affichés et la collection de messages sera vidée.
 * <p>
 * Pour personnaliser l'apparence des messages il faut définir les classes
 * fb-error et fb-info dans le CSS.
 * <p>
 * <pre>
 * Utilisation :
 * &lt;t:feedback t:id=&quot;feedback&quot;/&gt;
 * &#064;Component(id = &quot;feedback&quot;) private FeedBack feedback;
 * feedback.addInfo(&quot;message d'info&quot;);
 * feedback.addError(&quot;message d'erreur&quot;);
 * </pre>
 *
 * @author rannou
 * @author fdesbois
 * @version $Id$
 */
@IncludeStylesheet("feedback.css")
public class FeedBack {

    @Parameter(defaultPrefix = BindingConstants.LITERAL, value = "true")
    private boolean autoClear;

    /**
     * Messages d'erreur
     */
    private List<String> errorMessages;

    /**
     * Messages d'info
     */
    private List<String> infoMessages;

    private boolean errors;

    @Inject
    private ComponentResources resources;

    private boolean reload;

    public FeedBack() {
        errorMessages = new ArrayList<String>();
        infoMessages = new ArrayList<String>();
    }

    void beginRender(MarkupWriter writer) {

        // Rendu des messages d'erreur et vidage de la collection
        if (!errorMessages.isEmpty()) {
            errors = true;
            writer.element("div", "class", "fb-error");
            for (String message : errorMessages) {
                writer.write(message);
                writer.element("br");
                writer.end();
            }
            writer.end();
        }
        if (autoClear) {
            errorMessages.clear();
        }

        // Rendu des messages d'info et vidage de la collection
        if (!infoMessages.isEmpty()) {
            writer.element("div", "class", "fb-info");
            for (String message : infoMessages) {
                writer.write(message);
                writer.element("br");
                writer.end();
            }
            writer.end();
        }
        infoMessages.clear();

        resources.renderInformalParameters(writer);
    }

    void afterRender(MarkupWriter writer) {

    }

    /**
     * Ajout d'un message d'info.
     *
     * @param message le message
     */
    public void addInfo(String message) {
        if (!infoMessages.contains(message)) {
            infoMessages.add(message);
        }
    }

    /**
     * Ajout d'un message d'erreur.
     *
     * @param message le message
     */
    public void addError(String message) {
        if (!errorMessages.contains(message)) {
            errorMessages.add(message);
        }
    }

    public boolean hasErrors() {
        return errors;
    }

    @Log
    public void reload() {
        reload = true;
    }

    public boolean hasBeeanReloaded() {
        return reload;
    }

    public void clearErrors() {
        errorMessages.clear();
        reload = false;
    }
}
