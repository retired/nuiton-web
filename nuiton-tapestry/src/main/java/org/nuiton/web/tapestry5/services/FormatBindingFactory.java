/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.tapestry5.services;

import org.apache.tapestry5.Binding;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.ioc.Location;
import org.apache.tapestry5.services.BindingFactory;
import org.apache.tapestry5.services.BindingSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * From <a href="http://wiki.apache.org/tapestry/Tapestry5HowToAddMessageFormatBindingPrefix">Tapestry5HowTo</a>
 * <br>
 * <h2>AppModule contribution :</h2>
 * <pre>
 *   public static void contributeBindingSource(
 *           MappedConfiguration&lt;String, BindingFactory&gt; configuration,
 *           BindingSource bindingSource) {
 *       configuration.add("format", new FormatBindingFactory(bindingSource));
 *   }
 * </pre>
 * <p>
 * Created: 27 avr. 2010
 *
 * @author fdesbois
 * @version $Id$
 */
public class FormatBindingFactory
        implements BindingFactory {

    private static final String SEPARATOR = "=";
    private static final String DELIMITER = ",";
    private static final String KEY_PREFIX = BindingConstants.LITERAL;
    private static final String VALUE_PREFIX = BindingConstants.PROP;
    private final BindingSource bindingSource;

    public FormatBindingFactory(BindingSource bindingSource) {
        this.bindingSource = bindingSource;
    }

    @Override
    public Binding newBinding(String description, ComponentResources container, ComponentResources component,
                              String expression, Location location) {
        int separatorIndex = expression.indexOf(SEPARATOR);

        if (-1 == separatorIndex) {
            List<String> keys = Arrays.asList(expression.split(DELIMITER));

            ArrayList<Binding> keyBindings = createBindings(description, container, component, KEY_PREFIX, keys, location);

            boolean invariant = isInvariant(keyBindings);
            return new FormatBinding(location, container.getMessages(), invariant, keyBindings);
        }

        List<String> keys = Arrays.asList(expression.substring(0, separatorIndex).split(DELIMITER));
        ArrayList<Binding> keyBindings = createBindings(description, container, component, KEY_PREFIX, keys, location);

        List<String> values = Arrays.asList(expression.substring(separatorIndex + 1).split(DELIMITER));
        ArrayList<Binding> valueBindings = createBindings(description, container, component, VALUE_PREFIX, values,
                location);

        boolean invariant = isInvariant(keyBindings) && isInvariant(valueBindings);
        return new FormatBinding(location, container.getMessages(), invariant, keyBindings, valueBindings);
    }

    private ArrayList<Binding> createBindings(String description, ComponentResources container,
                                              ComponentResources component, String defaultPrefix,
                                              List<String> expressions, Location location) {
        ArrayList<Binding> bindings = new ArrayList<Binding>(expressions.size());

        for (String expression : expressions) {
            bindings.add(bindingSource.newBinding(description, container, component, defaultPrefix, expression, location));
        }

        return bindings;
    }

    private boolean isInvariant(ArrayList<Binding> bindings) {
        for (Binding binding : bindings) {
            if (!binding.isInvariant()) {
                return false;
            }
        }

        return true;
    }
}
