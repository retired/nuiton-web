/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * *##% 
 * Wao :: Web Interface
 * Copyright (C) 2009 - 2010 Ifremer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * ##%*
 */
package org.nuiton.web.tapestry5.mixins;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.ClientElement;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.InjectContainer;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

/**
 * From <a href="http://tinybits.blogspot.com/2009/05/update-zone-on-any-client-side-event.html">Ingesol Blog</a>
 *
 * @author ingesol
 * @author fdesbois
 * @version $Id$
 */
@Import(library = "zoneUpdater.js")
public class ZoneUpdater {

    public static final String PLACEHOLDER = "XXX";

    @Inject
    private ComponentResources resources;

    @Environmental
    private JavaScriptSupport javaScriptSupport;

    @Parameter(defaultPrefix = BindingConstants.LITERAL)
    private String clientEvent;

    @Parameter(defaultPrefix = BindingConstants.LITERAL, required = true)
    private String event;

    @InjectContainer
    private ClientElement element;

    @Parameter
    private Object[] context;

    @Parameter(defaultPrefix = BindingConstants.LITERAL)
    // To enable popups to fire events on this document, enter "document" here.
    private String listeningElement;

    @Parameter(defaultPrefix = BindingConstants.LITERAL, required = true)
    private String zone;

    protected Link createLink(Object[] context) {

        if (context == null) {
            context = new Object[]{PLACEHOLDER};
        } else {
            // To be replaced by javascript
            context = ArrayUtils.add(context, PLACEHOLDER);
        }

        return resources.createEventLink(event, context);

    }

    void afterRender() {
        String link = createLink(context).toAbsoluteURI();
        String elementId = element.getClientId();
        if (clientEvent == null) {
            clientEvent = event;
        }

        if (listeningElement == null) {
            listeningElement = "$('" + elementId + "')";
        }

        javaScriptSupport.addScript(
                "new ZoneUpdater('%s', %s, '%s', '%s', '%s', '%s')",
                elementId,
                listeningElement,
                clientEvent,
                link,
                zone,
                PLACEHOLDER
        );
    }
}
