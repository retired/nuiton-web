/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.tapestry5.services;

/**
 * This service is used to provide authentication managment for a final
 * application. The user type used in the application is needed. This service
 * need to be implemented and build in the AppModule of your application.
 * An implementation is available {@link ServiceAuthenticationImpl}
 * This service is used in {@link AbstractAuthenticationFilter} to verify
 * user rights for displaying pages using {@link #isAllowed(Class)} method.
 * See documentation site for more explanation of using this service and
 * authentication managment.
 * <br>
 * <h2>AppModule contribution :</h2>
 * Exemple with MyUser type for user and ServiceAuthenticationImpl for
 * implementation :
 * <pre>
 *  public ServiceAuthentication&lt;MyUser&gt; buildServiceAuthentication(
 *          ApplicationStateManager stateManager) {
 *      ServiceAuthentication&lt;yUser&gt; instance =
 *              new ServiceAuthenticationImpl(stateManager);
 *      return instance;
 *  }
 * </pre>
 * Contribute to ApplicationStateManager to instantiate MyUser automatically :
 * <pre>
 * public void contributeApplicationStateManager(
 *           MappedConfiguration&lt;Class&lt;?&gt;,
 *           ApplicationStateContribution&gt; configuration,
 *           final ServiceAuthentication&lt;MyUser&gt; serviceAuthentication) {
 *
 *      ApplicationStateCreator&lt;MyUser&gt; creator =
 *              new ApplicationStateCreator&lt;MyUser&gt;() {
 *
 *                  public MyUser create() {
 *                      return serviceAuthentication.getNewUserInstance();
 *                  }
 *              };
 *
 *      configuration.add(MyUser.class,
 *              new ApplicationStateContribution("session", creator));
 *  }
 * </pre>
 * <p>
 * <p>
 * Created: 3 mai 2010
 *
 * @param <U> user type
 * @author fdesbois
 * @version $Id$
 */
public interface ServiceAuthentication<U> {

    /**
     * Detect if user is connected.
     *
     * @return true if the user is connected, false otherwise.
     */
    boolean isUserConnected();

    /**
     * Get the current user connected.
     *
     * @return the user connected
     */
    U getUserConnected();

    /**
     * Set the connected user to {@code user}.
     *
     * @param user that will be connected
     */
    void setUserConnected(U user);

    /**
     * Check if the current user is allowed to display this {@code page}.
     *
     * @param page to check
     * @return true if the connected user is allowed, false otherwise
     */
    boolean isAllowed(Class<?> page);

    /**
     * Create a new instance of user.
     *
     * @return a new user.
     */
    U getNewUserInstance();

}
