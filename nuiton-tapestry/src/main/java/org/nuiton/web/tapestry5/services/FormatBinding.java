/*
 * #%L
 * Nuiton Web :: Nuiton Tapestry
 * %%
 * Copyright (C) 2010 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.web.tapestry5.services;

import org.apache.tapestry5.Binding;
import org.apache.tapestry5.internal.bindings.AbstractBinding;
import org.apache.tapestry5.ioc.Location;
import org.apache.tapestry5.ioc.Messages;

import java.util.ArrayList;
import java.util.List;

/**
 * From <a href="http://wiki.apache.org/tapestry/Tapestry5HowToAddMessageFormatBindingPrefix">Tapestry5HowTo</a>
 * <h2>Usage :</h2>
 * <pre>
 * ${format:key=value1,value2} or &lt;t:component t:parameter="format:key=value1,value2"&gt;
 * </pre>
 * <p>
 * Created: 27 avr. 2010
 *
 * @author fdesbois
 * @version $Id$
 * @see FormatBindingFactory
 */
public class FormatBinding extends AbstractBinding {

    private final Messages messages;
    private final boolean invariant;
    private final List<Binding> keyBindings;
    private final List<Binding> valueBindings;

    public FormatBinding(Location location, Messages messages, boolean invariant,
                         ArrayList<Binding> keyBindings, List<Binding> valueBindings) {
        super(location);

        this.messages = messages;
        this.invariant = invariant;
        this.keyBindings = keyBindings;
        this.valueBindings = valueBindings;
    }

    public FormatBinding(Location location, Messages messages, boolean invariant, ArrayList<Binding> keyBindings) {
        super(location);

        this.messages = messages;
        this.invariant = invariant;
        this.keyBindings = keyBindings;
        this.valueBindings = null;
    }

    @Override
    public Object get() {
        String key = "";
        for (Binding keyBinding : keyBindings) {
            key += keyBinding.get();
        }

        if (null == valueBindings) {
            return messages.get(key);
        }

        List<Object> values = new ArrayList<Object>(valueBindings.size());
        for (Binding valueBinding : valueBindings) {
            values.add(valueBinding.get());
        }

        return messages.format(key, values.toArray());
    }

    @Override
    public boolean isInvariant() {
        return this.invariant;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Class<?> getBindingType() {
        return String.class;
    }
}
